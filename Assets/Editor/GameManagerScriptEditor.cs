﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameStateManager))]
public class GameManagerScriptEditor : Editor {
	public override void OnInspectorGUI() {
		GameStateManager gameStateManager = (GameStateManager)target;
		if (GUILayout.Button("Edit Events")) {
			gameStateManager.SetEvents();
		}
		DrawDefaultInspector();
	}
}
