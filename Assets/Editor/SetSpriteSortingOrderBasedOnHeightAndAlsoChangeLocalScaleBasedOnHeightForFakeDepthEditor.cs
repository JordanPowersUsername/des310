﻿using System.Collections;
using UnityEngine;
using UnityEditor;

// but this time as a button, but not for scale because then defaultScale will change

[CustomEditor(typeof(SetSpriteSortingOrderBasedOnHeight))]
public class SetSpriteSortingOrderBasedOnHeightEditor : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		GUILayout.Label("Below button is just for looking nice in the editor.\nThe script will be applied during gameplay regardless.");
		if (GUILayout.Button("Editor - Set sprite sorting order based on current height")) {
			SetSpriteSortingOrderBasedOnHeight setSpriteSortingOrderBasedOnHeightAndAlsoChangeLocalScaleBasedOnHeightForFakeDepthComponent = (SetSpriteSortingOrderBasedOnHeight)target;
			SpriteRenderer spriteRenderer = setSpriteSortingOrderBasedOnHeightAndAlsoChangeLocalScaleBasedOnHeightForFakeDepthComponent.gameObject.GetComponent<SpriteRenderer>();
			SerializedObject serializedSpriteRenderer = new UnityEditor.SerializedObject(spriteRenderer);
			SerializedProperty sortingOrder = serializedSpriteRenderer.FindProperty("m_SortingOrder");
			serializedSpriteRenderer.Update();
			sortingOrder.intValue = -(int)(setSpriteSortingOrderBasedOnHeightAndAlsoChangeLocalScaleBasedOnHeightForFakeDepthComponent.transform.position.y * 10f);
			serializedSpriteRenderer.ApplyModifiedProperties();
		}
	}
}
