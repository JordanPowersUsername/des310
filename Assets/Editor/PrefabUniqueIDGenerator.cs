﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PrefabUniqueID))]
public class PrefabUniqueIDGenerator : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		GUILayout.Label("Make sure when instantiating Game Objects with this to:");
		GUILayout.Label("\tHave this prefab in the SaveAndLoad instantiatable prefab list");
		GUILayout.Label("\tAfter instantiation, set the saveAndLoad property - ");
		GUILayout.Label("\t(you can use the Instantiate function inside SaveAndLoad or do it manually)");

		SerializedObject serializedObject = new UnityEditor.SerializedObject(target);
		SerializedProperty serializedProperty = serializedObject.FindProperty("prefabUniqueID");

		if (serializedProperty.stringValue.Length == 0) {
			if (GUILayout.Button("Generate Unique ID for Prefab")) {
				serializedProperty.stringValue = System.Guid.NewGuid().ToString();
				serializedObject.ApplyModifiedProperties();
			}
		}
		else {
			GUILayout.Label("Prefab Unique ID: " + serializedProperty.stringValue);
			if (GUILayout.Button("Unset Unique ID for Prefab")) {
				serializedProperty.stringValue = "";
				serializedObject.ApplyModifiedProperties();
			}
		}
	}
}
