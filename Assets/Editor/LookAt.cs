﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Camera))]
public class LookAt : Editor {

	private Vector3 position = Vector3.zero;

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		Camera camera = (Camera)target;

		position = EditorGUILayout.Vector3Field("Target", position);
		if (GUILayout.Button("Look at")) {
			camera.transform.LookAt(position, Vector3.back);
		}
	}
}
