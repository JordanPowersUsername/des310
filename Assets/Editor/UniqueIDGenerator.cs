﻿using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UniqueID))]
public class UniqueIDGenerator : Editor {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();

		SerializedObject serializedObject = new UnityEditor.SerializedObject(target);
		SerializedProperty serializedProperty = serializedObject.FindProperty("uniqueID");

		if (serializedProperty.stringValue.Length == 0) {
			if (GUILayout.Button("Generate Unique ID")) {
				serializedProperty.stringValue = System.Guid.NewGuid().ToString();
				serializedObject.ApplyModifiedProperties();
			}
		}
		else {
			GUILayout.Label("Unique ID: " + serializedProperty.stringValue);
			if (GUILayout.Button("Unset Unique ID")) {
				serializedProperty.stringValue = "";
				serializedObject.ApplyModifiedProperties();
			}
		}
	}
}
