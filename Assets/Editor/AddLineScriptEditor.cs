﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AddLine))]
public class AddLineScriptEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		AddLine addLine = (AddLine)target;

		if (GUILayout.Button("Add Line"))
		{
			addLine.CreateLine();
		}

		if (GUILayout.Button("Add Line via Index"))
		{
			addLine.CreateLineInt();
		}

		if (GUILayout.Button("Remove Line via Index"))
		{ 
			addLine.RemoveLineInt();
		}

		if (GUILayout.Button("Remove Line via Message"))
		{
			addLine.RemoveLineString();
		}

		if (GUILayout.Button("Show a line via Index"))
		{
			addLine.ShowLineInt();
		}

		if (GUILayout.Button("Show a line via String"))
		{
			addLine.ShowLineString();
		}

		if (GUILayout.Button("Replace a line via Index"))
		{
			addLine.ReplaceLineInt();
		}
		
		if (GUILayout.Button("Update Indexes")) {
			addLine.SetIndexAll();
		}

		if (GUILayout.Button("Copy Choices by Index")) {
			addLine.CopyChoiceByIndex();
		}
	}
}
