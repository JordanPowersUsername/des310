﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuplicateKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable {
	public int Compare(TKey x, TKey y) {
		int result = y.CompareTo(x);
		if (result == 0) {
			return 1;
		}
		return result;
	}
}

public class IngameButtonManager : MonoBehaviour {
	[SerializeField]
	InputManager inputManager = null;
	[SerializeField]
	GameStateManager gameStateManager = null;

	SortedList<int, IngameButton> ingameButtons = new SortedList<int, IngameButton>(new DuplicateKeyComparer<int>());

	void Update() {
		// if tapped in movement state
		if (inputManager.state == InputManager.State.Tap &&
			gameStateManager.state == GameStateManager.GameState.Movement) {
			// go through all ingame buttons and call the first one that is tapped
			// since they are sorted in order of importance, the important buttons will be pressed first
			foreach (KeyValuePair<int, IngameButton> ingameButtonPair in ingameButtons) {
				// if can invoke button, do it
				if (ingameButtonPair.Value.TryInvoke(inputManager.worldPosition)) {
					// consume the tap
					inputManager.ConsumeTap();
					// stop
					return;
				}
			}
		}
	}

	public void AddIngameButtonToSortedList(int order, IngameButton ingameButton) {
		ingameButtons.Add(order, ingameButton);
	}

	public void RemoveIngameButtonFromSortedList(IngameButton ingameButton) {
		ingameButtons.RemoveAt(ingameButtons.IndexOfValue(ingameButton));
	}
}
