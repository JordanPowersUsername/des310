﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Love : MonoBehaviour {
	[SerializeField]
	GameStateManager gameStateManager = null;

	GameObject[] buttons;
	[SerializeField]
	string[] positivePhrases = null;
	int positiveIndex; // used when setting text as we want to use a bag randomiser
	int numberOfPositivePhrases;
	[SerializeField]
	string[] negativePhrases = null;
	int negativeIndex;
	int numberOfNegativePhrases;

	int positiveTaps = 0;
	[SerializeField]
	UnityEvent onPositive = null;
	[SerializeField]
	UnityEvent onWin = null;
	[SerializeField]
	UnityEvent onLose = null;

	bool win = false;
	bool lovePlantOnClose = false;

	void Awake() {
		// add all children (buttons) to buttons
		buttons = new GameObject[transform.childCount];
		for (int i = 0; i < transform.childCount; ++i) {
			buttons[i] = transform.GetChild(i).gameObject;
		}
		// start with both indexes at the end so that the arrays get shuffled
		positiveIndex = positivePhrases.Length;
		negativeIndex = negativePhrases.Length;
		// set number of positive and negative phrases to show
		if (buttons.Length % 2 != 0) {
			Debug.LogWarning("Love does NOT have a multiple of 2 length of Buttons!");
		}
		numberOfPositivePhrases = buttons.Length / 2;
		numberOfNegativePhrases = buttons.Length / 2;
		// set buttons
		SetButtons();
	}

	void SetButtons() {
		// shuffle buttons
		Shuffle(buttons);
		for (int i = 0; i < buttons.Length; ++i) {
			Button button = buttons[i].GetComponent<Button>();
			Text text = buttons[i].GetComponentInChildren<Text>();
			LoveButton loveButton = buttons[i].GetComponent<LoveButton>();
			// make buttons all interactable
			button.interactable = true;
			// set text to a positive or negative phrase
			bool positive = i < numberOfPositivePhrases;

			text.text = positive ? GetRandomPositivePhrase() : GetRandomNegativePhrase();
			// set boolean for whether button should be considered positive or negative
			loveButton.positive = positive;
		}
	}

	string GetRandomPositivePhrase() {
		// round total positive phrases down to nearest multiple of buttons of positive phrases
		// (ie, we have 19, we are rounding down to the nearest mutliple of 3, which is 18)
		// this is to ensure that we don't shuffle half way through setting buttons (and end up with duplicate phrases)
		int rounded = positivePhrases.Length - (positivePhrases.Length % numberOfPositivePhrases);
		// if at end of array rounded down
		if (positiveIndex >= rounded) {
			// go back to start of array
			positiveIndex = 0;
			// shuffle array
			Shuffle(positivePhrases);
		}
		// choose a random phrase
		return positivePhrases[positiveIndex++];
	}

	string GetRandomNegativePhrase() {
		// round total positive phrases down to nearest multiple of buttons of positive phrases
		// (ie, we have 19, we are rounding down to the nearest mutliple of 3, which is 18)
		// this is to ensure that we don't shuffle half way through setting buttons (and end up with duplicate phrases)
		int rounded = negativePhrases.Length - (negativePhrases.Length % numberOfNegativePhrases);
		// if at end of array rounded down
		if (negativeIndex >= rounded) {
			// go back to start of array
			negativeIndex = 0;
			// shuffle array
			Shuffle(negativePhrases);
		}
		// choose a random phrase
		return negativePhrases[negativeIndex++];
	}

	void Shuffle<T>(T[] array) {
		// shuffle array using Fisher–Yates shuffle
		for (int i = array.Length - 1; i > 0; --i) {
			// random number from 0 to i 
			int j = Random.Range(0, i);
			// swap elements
			T temporary = array[i];
			array[i] = array[j];
			array[j] = temporary;
		}
	}

	public void PositiveTapped() {
		// hooray
		++positiveTaps;
		onPositive.Invoke();
		if (positiveTaps == numberOfPositivePhrases) {
			// a winner is you!
			win = true;
			// disable in one second
			Invoke("Disable", 1f);
			// disable all buttons
			foreach (GameObject button in buttons) {
				button.GetComponent<Button>().interactable = false;
			}
			// love plant on close
			lovePlantOnClose = true;
		}
	}

	public void NegativeTapped() {
		// uh oh!
		win = false;
		// disable in one second
		Invoke("Disable", 1f);
		// disable all buttons
		foreach (GameObject button in buttons) {
			button.GetComponent<Button>().interactable = false;
		}
	}

	void Disable() {
		// this is done here instead of earlier so that animation can be seen
		if (lovePlantOnClose) {
			// love the plant!
			GameObject plant = gameStateManager.target;
			LoveHolder loveHolder = plant.GetComponent<LoveHolder>();
			loveHolder.hasLove = true;
		}
		// disable love minigame
		gameObject.SetActive(false);
		// reset game state
		gameStateManager.SetState(GameStateManager.GameState.Movement);

		if (win) {
			onWin.Invoke();
		}
		else {
			onLose.Invoke();
		}
	}

	public void Enable(GameObject plant) {
		// enable love minigame
		gameObject.SetActive(true);
		gameStateManager.SetState(GameStateManager.GameState.LoveMinigame, plant);
		// set all buttons
		SetButtons();
		// set positive taps to 0
		positiveTaps = 0;
		// by default, should NOT love the plant on close
		lovePlantOnClose = false;
	}
}
