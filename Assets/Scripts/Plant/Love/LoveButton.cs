﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoveButton : MonoBehaviour {
	public bool positive { get; set; } = false;

	Button button;
	Love love;
	void Awake() {
		button = GetComponent<Button>();
		love = GetComponentInParent<Love>();
	}

	public void OnTap() {
		// disable interaction with button
		button.interactable = false;
		// positive
		if (positive) {
			love.PositiveTapped();
		}
		// negative
		else {
			love.NegativeTapped();
		}
	}
}
