﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupLiquid : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	Sprite full = null;
	[SerializeField]
	Sprite empty = null;
	[SerializeField]
	Sprite dropped = null;

	[SerializeField]
	private bool isFull = false;

	[SerializeField]
	private PickUpHolder pickUpHolder = null;

	SpriteRenderer spriteRenderer;
	PickUp pickUp;
	void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		pickUp = GetComponent<PickUp>();
	}

	void Start() {
		UpdateSpriteAndTag();
	}

	public void UpdateSpriteAndTag() {
		spriteRenderer.sprite = pickUp.PickedUp ? (isFull ? full : empty) : dropped;
		tag = "Water " + (isFull ? "Full" : "Empty");
		pickUpHolder.UpdateInventoryImagesSprites();
	}

	public void Fill() {
		isFull = true;
		UpdateSpriteAndTag();
	}

	public void Empty() {
		isFull = false;
		UpdateSpriteAndTag();
	}

	public bool IsFull() {
		return isFull;
	}

	[System.Serializable]
	struct IsFullStruct {
		public bool isFull;
	}
	public string Save() {
		IsFullStruct isFullStruct;
		isFullStruct.isFull = isFull;
		return JsonUtility.ToJson(isFullStruct);
	}
	public void Load(string json) {
		IsFullStruct isFullStruct = JsonUtility.FromJson<IsFullStruct>(json);
		isFull = isFullStruct.isFull;
		UpdateSpriteAndTag();
	}
}
