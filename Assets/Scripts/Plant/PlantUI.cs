﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantUI : MonoBehaviour
{
	[SerializeField]
	InputManager input = null;
	[SerializeField]
	PickUpHolder pickUpHolder = null;
	[SerializeField]
	Love loveUI = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	FlagManager flagManager = null;
	[SerializeField]
	EnergyManager energyManager = null;

	[SerializeField]
	GameObject waterButton = null;
	[SerializeField]
	GameObject plantButton = null;
	[SerializeField]
	GameObject loveButton = null;
	[SerializeField]
	GameObject removeButton = null;

	[SerializeField]
	float spinInDegrees = 180f;
	[SerializeField]
	float distance = 1f;
	[SerializeField]
	float startAnimationLengthInSeconds = 2f;
	[SerializeField]
	float endAnimationLengthInSeconds = 0.5f;
	[SerializeField]
	float size = 6f;

	GameObject plantPot = null;

	bool opening = true;

	class Child {
		public Transform transform = null;
		public float angleOffset = 0f;
	}
	List<Child> children = new List<Child>();

	float startTime;
	public void OpenPlantUI(GameObject plantPot) {
		// set opening to true
		opening = true;

		// enable children's buttons
		foreach (Transform childTransform in transform) {
			childTransform.GetComponent<IngameButton>().enabled = true;
		}
		PlantPot plantPotScript = plantPot.GetComponent<PlantPot>();
		// can water plant pot if player has full cup of water
		bool canWater = pickUpHolder.TryFindPickUpInHandWithTag("Water Full");
		waterButton.SetActive(canWater);
		// can plant plant if plant pot has no plant in pot and player has seed
		bool canPlantPlant = !plantPotScript.plant && pickUpHolder.TryFindPickUpInHandWithTag("Seed");
		plantButton.SetActive(canPlantPlant);
		// can remove dead plant if plant pot has dead plant in it
		PlantGrow plantGrow = plantPotScript.plant ? plantPotScript.plant.GetComponent<PlantGrow>() : null;
		bool canRemoveDeadPlant = plantGrow ? plantGrow.dead : false;
		removeButton.SetActive(canRemoveDeadPlant);
		// can love plant if plant pot has plant in stage 1 -> 2
		bool canLovePlant = plantGrow ? plantGrow.stage == 1 && !plantGrow.dead : false;
		loveButton.SetActive(canLovePlant);

		// add active children to list
		// clear list
		children.Clear();
		int i = 0;
		// go through all children
		for (i = 0; i < transform.childCount; ++i) {
			Transform childTransform = transform.GetChild(i);
			// if child is active
			if (childTransform.gameObject.activeSelf) {
				// add it to the list and make it have a scale of 0 (so it doesn't appear for 1 frame)
				Child child = new Child();
				childTransform.localScale = Vector2.zero;
				child.transform = childTransform;
				children.Add(child);
			}
		}
		// go through list and set angle offset
		i = 0;
		foreach (Child child in children) {
			child.angleOffset = Mathf.Lerp(0f, 2f * Mathf.PI, (float)i / children.Count);
			++i;
		}

		// if list is empty, return
		if (children.Count == 0) {
			return;
		}

		// activate this
		gameObject.SetActive(true);

		// move to plant pot and set this plant pot to plant pot
		this.plantPot = plantPot;
		transform.position = plantPot.transform.position;
		// disable plant pot button
		plantPot.GetComponent<IngameButton>().enabled = false;

		// set start time to now
		startTime = Time.time;
	}

	AudioSource audioSource;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	void Update() {
		// if anywhere is tapped, close plant UI
		if (input.state == InputManager.State.Tap) {
			input.ConsumeTap();
			ClosePlantUI();
		}
		// use trigonometry to orbit children around this
		foreach (Child child in children) {
			float alpha = (Time.time - startTime) / (opening ? startAnimationLengthInSeconds : endAnimationLengthInSeconds);
			// use equation of a circle along with horizontal line with time,
			// so that animation starts fast, then smooths out
			// graph: https://www.desmos.com/calculator/wjq4so4rl2
			float value = 0f;
			if (opening) {
				value = Mathf.Sqrt(1f - Mathf.Pow(1f - Mathf.Min(1f, alpha), 2f));
			}
			else {
				// if finished closing, deactivate this
				if (alpha >= 1f) {
					gameObject.SetActive(false);
					return;
				}
				// use smooth step for closing
				value = Mathf.SmoothStep(1f, 0f, alpha);
			}

			// calculate angle
			float angle = Mathf.Deg2Rad * spinInDegrees * value + child.angleOffset;
			if (children.Count > 1) {
				// use trigonometry to calculate position
				child.transform.localPosition = new Vector2(
					Mathf.Cos(angle),
					Mathf.Sin(angle)
				) * distance * value;
			}
			else {
				// use trigonometry to calculate position
				child.transform.localPosition = new Vector2(
					Mathf.Cos(angle),
					Mathf.Sin(angle)
				) * distance * (1f - value);
			}
			

			// get bigger
			child.transform.localScale = size * value * Vector2.one;
		}
	}

	public void ClosePlantUI() {
		// set opening to false
		opening = false;
		// disable children's buttons
		foreach (Transform childTransform in transform) {
			childTransform.GetComponent<IngameButton>().enabled = false;
		}
		// set animation start time to now
		startTime = Time.time;
		// enable plant pot button
		plantPot.GetComponent<IngameButton>().enabled = true;
		energyManager.LockOut();
	}

	public void PlantSeed() {
		// find seeds
		GameObject seed = pickUpHolder.TryFindPickUpInHandWithTag("Seed");
		// if there are no seeds, or there is already a plant in the plant pot then return
		PlantPot plantPotScript = plantPot.GetComponent<PlantPot>();
		if (seed == null || plantPotScript.plant) {
			Debug.LogWarning("Can't plant, there is already plant in the plant pot");
			return;
		}
		if (!energyManager.TryUseUpEnergy(1)) {
			energyManager.LockOut();
			return;
		}
		// plant prefab
		GameObject plant = Instantiate(seed.GetComponent<Seed>().prefab, plantPot.transform);
		PlantGrow plantGrow = plant.GetComponent<PlantGrow>();
		plantGrow.plantUI = this;
		plantGrow.gameStateManager = gameStateManager;
		plantGrow.flagManager = flagManager;
		// set plant pot plant to new instantiated plant prefab
		plantPotScript.plant = plant;
		// destroy the seeds
		Destroy(seed);

		flagManager.SetFlag("Pot", 4);
	}

	public void WaterPlant() {
		// find full cup of water
		GameObject water = pickUpHolder.TryFindPickUpInHandWithTag("Water Full");
		// if there is no full cup of water return
		if (water == null) {
			Debug.LogWarning("Can't water plant pot, player has no water");
			return;
		}
		if (!energyManager.TryUseUpEnergy(1)) {
			energyManager.LockOut();
			return;
		}
		// water plant pot
		++plantPot.GetComponent<Hydration>().Water;
		water.GetComponent<CupLiquid>().Empty();
		// play sound
		audioSource.Play();

		flagManager.SetFlag("Pot", 5);
	}

	public void RemovePlant() {
		// check if there is a plant in the pot
		PlantPot plantPotScript = plantPot.GetComponent<PlantPot>();
		if (plantPotScript.plant) {
			if (!energyManager.TryUseUpEnergy(1)) {
				energyManager.LockOut();
				return;
			}
			// if there is, destroy plant and remove all water
			Destroy(plantPotScript.plant);
			plantPot.GetComponent<Hydration>().Water = 0;
			return;
		}
	}

	public void LovePlant() {
		// check if there is a plant in the pot in stage 1 -> 2 and not dead
		PlantPot plantPotScript = plantPot.GetComponent<PlantPot>();
		PlantGrow plantGrow = plantPotScript.plant ? plantPotScript.plant.GetComponent<PlantGrow>() : null;
		if (plantGrow && plantGrow.stage == 1 && !plantGrow.dead) {
			// try love it
			loveUI.Enable(plantPotScript.plant);
		}
	}
}
