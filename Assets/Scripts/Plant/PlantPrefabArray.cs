﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantPrefabArray : MonoBehaviour {
	[SerializeField]
	GameObject[] plantPrefabs = null;

	public GameObject GetPlant(int index) {
		return plantPrefabs[index];
	}
}
