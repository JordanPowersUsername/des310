﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayNoteOnKeyPressOrTap : MonoBehaviour {
	[SerializeField]
	KeyCode keyCode = KeyCode.None;
	[SerializeField]
	AudioClip audioClip = null;

	AudioSource audioSource;
	PianoCheats pianoCheats;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
		// if we have a mouse, we probably have a keyboard
		if (Input.mousePresent) {
			// set text child's text to the key that needs to be pressed
			string keyCodeAsString = keyCode.ToString();
			// use only last number (needed for "Alpha" keys)
			keyCodeAsString = keyCodeAsString.Substring(keyCodeAsString.Length - 1);
			transform.GetChild(0).GetComponent<Text>().text = keyCodeAsString;
		}
		else {
			// otherwise, hide text child
			transform.GetChild(0).gameObject.SetActive(false);
		}
		pianoCheats = transform.parent.GetComponent<PianoCheats>();
	}

	void Update() {
		if (Input.GetKeyDown(keyCode)) {
			PlayNote();
		}
	}

	public void PlayNote() {
		audioSource.PlayOneShot(audioClip);
		pianoCheats.TryIncrementCheatProgression(name);
	}
}
