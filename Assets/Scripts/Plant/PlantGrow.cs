﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantGrow : MonoBehaviour {
	public GameStateManager gameStateManager = null;
	public FlagManager flagManager = null;

	public PlantUI plantUI { set; private get; } = null;
	[SerializeField]
	private int plantPrefabIndex = 0;
	[SerializeField]
	float secondsUntilNextStage = 0f;
	// alarm counts down, and when it reaches 0, the next stage occurs, and the alarm resets using secondsUntilNextStage
	public float alarm { set; get; } = 0f;
	public int stage { set; get; } = 0;
	public bool dead { set; get; } = false;
	[SerializeField]
	PlantPot.Sunlight sunlightNeeded = PlantPot.Sunlight.lotsOfSunlight;
	[SerializeField]
	bool musicNeeded = false;
	[SerializeField]
	bool loveNeeded = true;
	[SerializeField]
	int waterNeeded = 1;

	PlantPot plantPot;
	Hydration hydration;
	LoveHolder loveHolder;
	Animator animator;
	AudioSource audioSource;
	void Awake() {
		plantPot = GetComponentInParent<PlantPot>();
		hydration = GetComponentInParent<Hydration>();
		loveHolder = GetComponent<LoveHolder>();
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
		// note: for below to work when loading before room has been active at least once, you need to set the sorting order of the plant pot in the editor
		GetComponent<SpriteRenderer>().sortingOrder = transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1;
	}

	void Start() {
		// if alarm is not set yet (by loading), then we've been newly created
		if (alarm == 0f) {
			alarm = secondsUntilNextStage;
		}
		UpdateSprite();
		flagManager.SetFlag("Plant", 0);
	}

	void FixedUpdate() {
		if (flagManager.GetFlag("Gardener") < 5) {
			return;
		}
		// if at final stage, or dead, or plant ui is active and enabled,
		// or game is locked out, return
		if (stage >= 2 || dead || plantUI.isActiveAndEnabled ||
			gameStateManager.state == GameStateManager.GameState.LockedOut
		) {
			return;
		}
		// if stage is 0 -> 1
		if (stage == 0) {
			// if enough time has passed
			alarm -= Time.deltaTime;
			if (alarm <= 0f) {
				// if the correct conditions are met during stage zero -> one
				if (plantPot.sunlight == sunlightNeeded &&
					plantPot.music == musicNeeded &&
					hydration.Water == waterNeeded) {
					// move onto next stage and set sprite to that stage
					++stage;
					UpdateSprite();
					// play sound
					audioSource.Play();
					// reset alarm
					alarm = secondsUntilNextStage;
				}
				// otherwise the plant is dead
				else {
					dead = true;
					UpdateSprite();
				}
			}
		}
		// else stage is 1 -> 2
		else {
			// if the plant has drowned
			if (hydration.Water > waterNeeded) {
				// kill plant
				dead = true;
				UpdateSprite();
			}
			// else if received love
			else if (loveHolder.hasLove == loveNeeded) {
				// move onto next stage and set sprite to that stage
				++stage;
				UpdateSprite();
				// play sound
				audioSource.Play();
			}
		}
	}

	public void ForceGrow() {
		// move onto next stage and set sprite to that stage
		++stage;
		UpdateSprite();
		// play sound
		audioSource.Play();
		// reset alarm
		alarm = secondsUntilNextStage;
	}

	public void UpdateSprite() {
		// animator is null if Active() hasn't been called (ie you load from in the elevator without ever moving out of it)
		if (animator != null) {
			if (dead) {
				animator.SetTrigger("Death");
				flagManager.SetFlag("Plant", 1);
			}
			else if (stage > 0) {
				animator.SetInteger("Stage", stage);
				if (stage == 2) {
					flagManager.SetFlag("Plant", 2);
				}
			}
		}
	}
	public int GetPlantPrefabIndex() {
		return plantPrefabIndex;
	}
}
