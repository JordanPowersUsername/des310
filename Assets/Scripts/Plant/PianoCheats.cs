﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PianoCheats : MonoBehaviour {
	int cheatProgression = 0;
	[SerializeField]
	string[] enableCheats = {
		"G4",
		"Gb4",
		"Eb4",
		"A3",
		"Ab3",
		"E4",
		"Ab4",
		"C4" // if only the piano had one more octave
	};
	[SerializeField]
	UnityEvent onCheatsEnabled = null;

	AudioSource audioSource;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}
	public void TryIncrementCheatProgression(string key) {
		if (enableCheats[cheatProgression] == key) {
			++cheatProgression;
			if (cheatProgression == enableCheats.Length) {
				audioSource.Play();
				onCheatsEnabled.Invoke();
				cheatProgression = 0;
			}
		}
		else {
			cheatProgression = 0;
		}
	}
}
