﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// invoked by Franny
public class SeedSpawner : MonoBehaviour {
	[SerializeField]
	SaveAndLoad saveAndLoad = null;
	[SerializeField]
	PickUpHolder pickUpHolder = null;
	[SerializeField]
	FlagManager flagManager = null;
	[SerializeField]
	GameObject[] prefabs;
	public void Start() {
		if (!pickUpHolder.TryFindPickUpWithTag("Seed")) {
			flagManager.SetFlag("Plant", 1);
		}
	}
	
	public void SpawnSeedOfApprpriateLevel() {
		switch(flagManager.GetFlag("Seed")) {
		case 0:
			SpawnSeed(prefabs[0]);
			break;
		case 1:
			SpawnSeed(prefabs[1]);
			break;
		case 2:
			SpawnSeed(prefabs[2]);
			break;
		case 3:
			SpawnSeed(prefabs[3]);
			break;
		case 4:
			SpawnSeed(prefabs[4]);
			break;
		}
	}
	public void SpawnSeed(GameObject prefab) {
		// if their is a free slot in the bottom row of the inventory
		if (pickUpHolder.HasFreeSlot(false)) {
			// instantiate and add to saveAndLoad's hashSet
			GameObject newSeed = saveAndLoad.InstantiateAndAddToInstantiatedObjectsToSaveAndLoad(prefab, transform);
			// pick up the spawned seed
			newSeed.GetComponent<PickUp>().TryPickUp();
		}
	}
}
