﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSource : MonoBehaviour {
	[SerializeField]
	InputManager input = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	PickUpHolder pickUpHolder = null;
	[SerializeField]
	MoveAlongPath moveAlongPath = null;

	
	BoxCollider2D boxCollider2D;
	Transform targetTransform = null;
	AudioSource audioSource;
	void Awake() {
		boxCollider2D = GetComponent<BoxCollider2D>();
		audioSource = GetComponent<AudioSource>();
		// if we have a child with the tag Pathfinding, use it as a target position to walk to first
		foreach (Transform child in transform) {
			if (child.CompareTag("Pathfinding")) {
				targetTransform = child;
				break;
			}
		}
	}

	void Update() {
		// if this is tapped during movement gamestate
		if (input.state == InputManager.State.Tap &&
			boxCollider2D.OverlapPoint(input.worldPosition) &&
			gameStateManager.state == GameStateManager.GameState.Movement) {

			// if player has any empty cups of water in their hands
			if (pickUpHolder.TryFindPickUpInHandWithTag("Water Empty")) {
				// consume tap and move player to object
				input.ConsumeTap();
				gameStateManager.SetState(GameStateManager.GameState.MoveToTarget, gameObject);
				moveAlongPath.PathfindAndStartPath(targetTransform.position);
			}
		}
		// if player has arrived at this
		if (!moveAlongPath.enabled &&
			gameStateManager.state == GameStateManager.GameState.MoveToTarget &&
			gameStateManager.target == gameObject) {
			// let the player resume normal movement
			gameStateManager.SetState(GameStateManager.GameState.Movement);
			// check player still has empty cup (they might have dropped it on the way)
			CupLiquid emptyCupOfWater = pickUpHolder.TryFindPickUpInHandWithTag("Water Empty").GetComponent<CupLiquid>();
			// if they do
			if (emptyCupOfWater != null) {
				// fill the cup of water
				emptyCupOfWater.Fill();
				// play sound
				audioSource.Play();
			}
		}
	}
}
