﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantPot : MonoBehaviour {
	public enum Sunlight {
		lotsOfSunlight,
		littleSunlight,
		noSunlight
	}

	public Sunlight sunlight;

	public bool music;

	public GameObject plant = null;

	public void ForceGrowChild() {
		if (plant) {
			plant.GetComponent<PlantGrow>().ForceGrow();
		}
	}
}
