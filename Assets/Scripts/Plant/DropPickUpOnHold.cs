﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropPickUpOnHold : MonoBehaviour {
	[SerializeField]
	InputManager input = null;
	bool alreadyDropped = false;

	PickUpHolder pickUpHolder;
	void Awake() {
		pickUpHolder = GetComponent<PickUpHolder>();
	}

	void Update() {
		if (input.state == InputManager.State.Hold) {
			if (!alreadyDropped) {
				alreadyDropped = true;
				pickUpHolder.DropPickUp();
			}
		}
		else {
			alreadyDropped = false;
		}
	}
}
