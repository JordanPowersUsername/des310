﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPianoOnTap : MonoBehaviour {
	[SerializeField]
	InputManager input = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	MoveAlongPath moveAlongPath = null;
	[SerializeField]
	GameObject pianoUI = null;


	BoxCollider2D boxCollider2D;
	void Awake() {
		boxCollider2D = GetComponent<BoxCollider2D>();
	}

	void Update() {
		// if this is tapped during movement gamestate
		if (input.state == InputManager.State.Tap &&
			boxCollider2D.OverlapPoint(input.worldPosition) &&
			gameStateManager.state == GameStateManager.GameState.Movement) {

			// consume tap and move player to object
			input.ConsumeTap();
			gameStateManager.SetState(GameStateManager.GameState.MoveToTarget, gameObject);
			moveAlongPath.PathfindAndStartPath(transform.position + new Vector3(0.625f, -0.275f, 0f));
		}
		// if player has arrived at this
		if (!moveAlongPath.enabled &&
			gameStateManager.state == GameStateManager.GameState.MoveToTarget &&
			gameStateManager.target == gameObject) {
			// start Playing Piano game state
			gameStateManager.SetState(GameStateManager.GameState.PlayingPiano);
			// activate Piano UI
			pianoUI.SetActive(true);
		}
	}

	public void StopPlayingPiano() {
		// resume regular movement game state
		gameStateManager.SetState(GameStateManager.GameState.Movement);
		// deactivate Piano UI
		pianoUI.SetActive(false);
	}
}
