﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hydration : MonoBehaviour {
	[SerializeField]
	Sprite dryPot = null;
	[SerializeField]
	Sprite wetPot = null;

	SpriteRenderer spriteRenderer;
	void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	private int water;
	public int Water {
		get {
			return water;
		}
		set {
			water = value;
			if (water >= 1) {
				spriteRenderer.sprite = wetPot;
			}
			else {
				spriteRenderer.sprite = dryPot;
			}
		}
	}
}