﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : MonoBehaviour, ISaveableAndLoadable {
	public string Save() {
		return prefab.GetComponent<PrefabUniqueID>().prefabUniqueID;
	}
	public void Load(string json) {
		PrefabUniqueID prefabUniqueID = GetComponent<PrefabUniqueID>();
		prefab = prefabUniqueID.saveAndLoad.GetPrefabFromPrefabUniqueID(json);
	}
	public GameObject prefab = null;
}
