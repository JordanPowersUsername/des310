﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour, ISaveableAndLoadable {
	// static variable, you DON'T need to store a TimeManager script to use this
	// (for saving and loading though, TimeManager is stored)
	// this is the seconds between when the game was last loaded and when the game was last saved
	// reset to 0 on New Game
	public static double secondsBetweenLastLoadAndLastSave { get; private set; } = 0;
	// save and load total seconds elapsed on save
	[System.Serializable]
	struct Properties {
		public double secondsSinceEpochSaveOccuredAt;
	}
	void Start() {
		// if not loading on gameplay scene enter (ie new game)
		if (!StaticVariablesToPassBetweenSceneTransitions.loadOnGameplaySceneEnter) {
			// reset timer
			secondsBetweenLastLoadAndLastSave = 0f;
		}
	}
	public string Save() {
		Properties properties = new Properties();
		properties.secondsSinceEpochSaveOccuredAt = GetSecondsSinceEpoch();
		return JsonUtility.ToJson(properties);
	}
	public void Load(string json) {
		Properties properties = JsonUtility.FromJson<Properties>(json);
		secondsBetweenLastLoadAndLastSave = GetSecondsSinceEpoch() - properties.secondsSinceEpochSaveOccuredAt;
	}
	// static function, you DON'T need to store a TimeManager script to use this
	public static double GetSecondsSinceEpoch() {
		System.DateTime epoch = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		return (System.DateTime.UtcNow - epoch).TotalSeconds;
	}
}
