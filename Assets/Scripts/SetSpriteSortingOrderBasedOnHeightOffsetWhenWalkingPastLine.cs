﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ie, the office desk, since when on the bottom left you should be in front and on the bottom right you should be behind

public class SetSpriteSortingOrderBasedOnHeightOffsetWhenWalkingPastLine : MonoBehaviour {
    [SerializeField]
    float offset = 0f;
    [SerializeField]
    float x = 0f;
    enum Direction {
        Left,
        Right
    }
    [SerializeField]
    Direction direction = Direction.Left;
    [SerializeField]
    Transform playerTransform;

    SetSpriteSortingOrderBasedOnHeight parentsSetSpriteSortingOrderBasedOnHeight;
    void Awake() {
        parentsSetSpriteSortingOrderBasedOnHeight = transform.parent.GetComponent<SetSpriteSortingOrderBasedOnHeight>();
    }

    void FixedUpdate() {
        if ((direction == Direction.Left && playerTransform.position.x < x) || (direction == Direction.Right && playerTransform.position.x >= x)) {
            parentsSetSpriteSortingOrderBasedOnHeight.offset = offset;
        }
    }
}
