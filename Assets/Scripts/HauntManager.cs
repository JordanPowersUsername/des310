﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HauntManager : MonoBehaviour
{
	[SerializeField]
	Haunt[] haunt = null;
	public int lengthOfHaunting = 10;

	private void Haunting() {
		// for each haunted object
		foreach (Haunt hauntedObject in haunt) {
			// if not haunted, continue to next haunted object
			if (!hauntedObject.haunting) {
				continue;
			}
			// otherwise, if enough time has passed, end haunt
			if ((int)TimeManager.GetSecondsSinceEpoch() >= hauntedObject.timeToNotHaunt) {
				hauntedObject.EndHaunt();
			}
		}
		// if all haunted objects are no longer haunted
		bool noneHaunted = true;
		foreach (Haunt hauntedObject in haunt) {
			if (hauntedObject.haunting) {
				noneHaunted = false;
				break;
			}
		}
		if (noneHaunted) {
			// cancle invoke
			CancelInvoke("Haunting");
			return;
		}
	}
}
