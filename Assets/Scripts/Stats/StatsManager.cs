﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
	public enum Stats{
		Care,
		Compassion,
		Competence,
		Comunication,
		Courage,
		Commitment
	}
	public int[] sixCs = new int[6];

	public void IncreaseStats(Stats stat, int num) {
		sixCs[(int)stat] += num;
	}

	public int GetStats(Stats stat) {
		return sixCs[(int)stat];
	}

	public void IncreaseCare(int num) {
		sixCs[0] += num;
	}

	public void IncreaseCompassion(int num) {
		sixCs[1] += num;
	}

	public void IncreaseCompetence(int num) {
		sixCs[2] += num;
	}

	public void IncreaseCommunication(int num) {
		sixCs[3] += num;
	}

	public void IncreaseCourage(int num) {
		sixCs[4] += num;
	}

	public void IncreaseCommitment(int num) {
		sixCs[5] += num;
	}
}
