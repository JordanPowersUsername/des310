﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSliderBasedOnStat : MonoBehaviour
{
	[SerializeField]
	StatsManager statsManager = null;
	[SerializeField]
	StatsManager.Stats stat = StatsManager.Stats.Care;

	Slider slider;
	void Awake() {
		slider = GetComponent<Slider>();
	}

	void FixedUpdate() {
		slider.value = statsManager.GetStats(stat);
	}
}
