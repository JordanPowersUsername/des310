﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatUI : MonoBehaviour {
	[SerializeField]
	private GameStateManager gameStateManager = null;
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private FlagManager flagManager = null;

	void Update() {
		// tap to hide
		if (inputManager.state == InputManager.State.Tap) {
			inputManager.ConsumeTap();
			flagManager.SetFlag("Stats", 2);
			Hide();
		}
	}

	public void Show() {
		// activate and set game state to showing stats
		gameObject.SetActive(true);
		flagManager.SetFlag("Stats", 1);
		gameStateManager.SetState(GameStateManager.GameState.ShowingStats);
	}

	void Hide() {
		// deactivate and set game state to default (movement)
		gameObject.SetActive(false);
		gameStateManager.SetState(GameStateManager.GameState.Movement);
	}
}
