﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveAndLoad : MonoBehaviour {
	// sometimes something big will change related to saving and loading.
	// when this happens, old loads might try to load when they are not compatible.
	// changing the version will force old load's not to work and vice versa.
	static uint version = 3;

	//private Dictionary<string, GameObject> objectsToSaveAndLoad = new Dictionary<string, GameObject>();
	private SortedList<int, GameObject> objectsToSaveAndLoad = new SortedList<int, GameObject>(new DuplicateKeyComparer<int>());

	// list used instead of dictionary since Unity doesn't display dictionary in inspector
	[SerializeField]
	private List<GameObject> instantiatablePrefabs = null;
	private HashSet<GameObject> instantiatedObjectsToSaveAndLoad = new HashSet<GameObject>();

	[System.Serializable]
	struct UniqueIDWithProperties {
		public string uniqueID;
		public Properties[] properties;
	}

	[System.Serializable]
	struct Properties {
		public string componentName;
		// json stored as string inside another json because I couldn't find a way to store this data in a way ToJson was happy with
		public string json;
	}

	[System.Serializable]
	struct InstantiatedGameObjectWithProperties {
		public string prefabUniqueID;
		public Properties[] properties;
	}

	struct GameObjects {
		public uint versionNeededToLoad;
		public UniqueIDWithProperties[] gameObjects;
		public InstantiatedGameObjectWithProperties[] instantiatedGameObjects;
	}

	// get template stuff, used when instantiating objects
	IngameButton templateIngameButton;
	PickUp templatePickUp;
	void Awake() {
		templateIngameButton = GetComponent<IngameButton>();
		templatePickUp = GetComponent<PickUp>();
	}

	// pick up holder for dropping everything on load
	[SerializeField]
	PickUpHolder pickUpHolder = null;

	public void Save() {
		// create gameObjects struct, which stores all the Unique ID's along with the properties that are to be saved
		GameObjects gameObjects;
		gameObjects.versionNeededToLoad = version;
		gameObjects.gameObjects = new UniqueIDWithProperties[objectsToSaveAndLoad.Count];
		// go through each object to be saved
		int gameObjectIndex = 0;
		foreach (KeyValuePair<int, GameObject> gameObjectPair in objectsToSaveAndLoad) {
			// set unique ID using objectsToSaveAndLoad dictionary key
			gameObjects.gameObjects[gameObjectIndex].uniqueID = gameObjectPair.Value.GetComponent<UniqueID>().uniqueID;

			// get all saveable and loadable components
			ISaveableAndLoadable[] saveableAndLoadables = gameObjectPair.Value.GetComponents<ISaveableAndLoadable>();
			if (saveableAndLoadables.Length == 0) {
				Debug.LogError("Tried to save " + gameObjectPair.Value.name + " but it has nothing to save :(");
			}

			// update values in interfaces and set jsons in struct
			gameObjects.gameObjects[gameObjectIndex].properties = new Properties[saveableAndLoadables.Length];

			int propertiesIndex = 0;
			foreach (ISaveableAndLoadable saveableAndLoadable in saveableAndLoadables) {
				gameObjects.gameObjects[gameObjectIndex].properties[propertiesIndex].componentName = saveableAndLoadable.GetType().Name;
				gameObjects.gameObjects[gameObjectIndex].properties[propertiesIndex].json = saveableAndLoadable.Save();
				++propertiesIndex;
			}

			++gameObjectIndex;
		}
		// go through each instantiated object to be saved
		gameObjects.instantiatedGameObjects = new InstantiatedGameObjectWithProperties[instantiatedObjectsToSaveAndLoad.Count];
		int instantiatedGameObjectIndex = 0;
		foreach (GameObject instantiatedGameObject in instantiatedObjectsToSaveAndLoad) {
			
			// get all saveable and loadable components
			ISaveableAndLoadable[] saveableAndLoadables = instantiatedGameObject.GetComponents<ISaveableAndLoadable>();
			if (saveableAndLoadables.Length == 0) {
				Debug.LogError("Tried to save instantiated " + instantiatedGameObject.name + " but it has nothing to save :(");
			}

			// update values in interfaces and set jsons in struct
			gameObjects.instantiatedGameObjects[instantiatedGameObjectIndex].prefabUniqueID = instantiatedGameObject.GetComponent<PrefabUniqueID>().prefabUniqueID;
			gameObjects.instantiatedGameObjects[instantiatedGameObjectIndex].properties = new Properties[saveableAndLoadables.Length];

			int propertiesIndex = 0;
			foreach (ISaveableAndLoadable saveableAndLoadable in saveableAndLoadables) {
				gameObjects.instantiatedGameObjects[instantiatedGameObjectIndex].properties[propertiesIndex].componentName = saveableAndLoadable.GetType().Name;
				gameObjects.instantiatedGameObjects[instantiatedGameObjectIndex].properties[propertiesIndex].json = saveableAndLoadable.Save();
				++propertiesIndex;
			}

			++instantiatedGameObjectIndex;
		}
		// open file for writing
		string path = Application.persistentDataPath + "/save.json";
		StreamWriter file = new StreamWriter(path, false);
		// write
		file.WriteLine(JsonUtility.ToJson(gameObjects, true));
		// close file
		file.Close();
	}

	public void Load() {
		// check file exists
		string path = Application.persistentDataPath + "/save.json";
		// if file does NOT exist, return
		if (!System.IO.File.Exists(path)) {
			Debug.LogError("Trying to load a save file, when one doesn't exist!");
			return;
		}
		// open file for reading
		StreamReader file = new StreamReader(path);
		// read into struct
		GameObjects gameObjects = JsonUtility.FromJson<GameObjects>(file.ReadToEnd());
		// close file
		file.Close();
		// return if loaded version is NOT compatible with current version
		if (gameObjects.versionNeededToLoad != version) {
			Debug.LogWarning("Incompatible version ("+ gameObjects.versionNeededToLoad + ") tried to load");
			Debug.LogWarning("Current SaveAndLoad needs version " + version);
			return;
		}

		// now apply data from struct into the actual game objects
		// destroy all current instantiated game objects
		foreach (GameObject gameObject in instantiatedObjectsToSaveAndLoad) {
			Destroy(gameObject);
		}
		instantiatedObjectsToSaveAndLoad.Clear();
		// drop everything in pick up holder
		pickUpHolder.DropEverything();
		// go through each InstantiatedGameObjectWithProperties in struct
		foreach (InstantiatedGameObjectWithProperties instantiatedGameObjectWithProperties in gameObjects.instantiatedGameObjects) {
			GameObject prefabToInstantiate = GetPrefabFromPrefabUniqueID(instantiatedGameObjectWithProperties.prefabUniqueID);
			if (prefabToInstantiate == null) {
				continue;
			}
			// instantiate from prefab and add to instantiatedObjectsToSaveAndLoad
			GameObject instantiatedGameObject = InstantiateAndAddToInstantiatedObjectsToSaveAndLoad(prefabToInstantiate);
			// invoke the load function on all saved components
			// go through all properties saved
			foreach (Properties properties in instantiatedGameObjectWithProperties.properties) {
				// get the component specified in the properties (there can not be two or more with the same name, please do not do that)
				ISaveableAndLoadable component = (ISaveableAndLoadable)instantiatedGameObject.GetComponent(properties.componentName);
				// make sure the component exists
				if (component == null) {
					Debug.LogError("Tried to load an saveable and loadable component with name " + properties.componentName + " in instantiated " + gameObject.name + " but no saveable and loadable components with that name are in the instantiated game object");
					return;
				}
				// load to properties for that component
				component.Load(properties.json);
			}
		}
		// go through each UniqueIDWithProperties in struct
		foreach (UniqueIDWithProperties uniqueIDWithProperties in gameObjects.gameObjects) {
			// check that unique id is in dictionary of objects to save and load
			GameObject gameObject = null;
			foreach (KeyValuePair<int, GameObject> gameObjectPair in objectsToSaveAndLoad) {
				if (gameObjectPair.Value.GetComponent<UniqueID>().uniqueID == uniqueIDWithProperties.uniqueID) {
					gameObject = gameObjectPair.Value;
					break;
				}
			}
			// make sure object exists
			if (!gameObject) {
				Debug.LogError("Tried to load an object with ID " + uniqueIDWithProperties.uniqueID + " but no objects with that ID are in the dictionary of objects to be saved and loaded.");
				return;
			}
			// invoke the load function on all saved components
			// go through all properties saved
			foreach (Properties properties in uniqueIDWithProperties.properties) {
				// get the component specified in the properties (there can not be two or more with the same name, please do not do that)
				ISaveableAndLoadable component = (ISaveableAndLoadable)gameObject.GetComponent(properties.componentName);
				// make sure the component exists
				if (component == null){
					Debug.LogError("Tried to load an saveable and loadable component with name " + properties.componentName + " in " + gameObject.name + " but no saveable and loadable components with that name are in the game object");
					return;
				}
				// load to properties for that component
				component.Load(properties.json);
			}
		}
	}

	public GameObject GetPrefabFromPrefabUniqueID(string prefabUniqueID) {
		if (prefabUniqueID == "") {
			Debug.LogError("PrefabUniqueID is not set.");
			return null;
		}
		foreach (GameObject prefab in instantiatablePrefabs) {
			if (prefab.GetComponent<PrefabUniqueID>().prefabUniqueID == prefabUniqueID) {
				return prefab;
			}
		}
		Debug.LogError("Can't find Prefab with prefabUniqueID of (" + prefabUniqueID + ") in list of instantiatable prefabs.");
		return null;
	}

	public void RemoveInstantiatedObjectFromHashSetOfInstantiatedObjectsToSaveAndLoad(GameObject gameObject) {
		instantiatedObjectsToSaveAndLoad.Remove(gameObject);
	}

	public void AddObjectToListOfObjectsToSaveAndLoad(int sortOrder, GameObject gameObject) {
		objectsToSaveAndLoad.Add(sortOrder, gameObject);
	}

	// instantiates and adds to instantiatedObjectsToSaveAndLoad, sets up components based on templates, and set's PrefabUniqueID's save and load to this
	public GameObject InstantiateAndAddToInstantiatedObjectsToSaveAndLoad(GameObject prefab, Transform transform = null) {
		GameObject gameObject = Instantiate(prefab, transform);
		instantiatedObjectsToSaveAndLoad.Add(gameObject);
		// if instantiated object has ingame button, use the template
		IngameButton ingameButton;
		if (gameObject.TryGetComponent<IngameButton>(out ingameButton)) {
			ingameButton.Clone(templateIngameButton);
		}
		// if instantiated object has pick up, use the template
		PickUp pickUp;
		if (gameObject.TryGetComponent<PickUp>(out pickUp)) {
			pickUp.Clone(templatePickUp);
		}
		// set PrefabUniqueID's save and load to this
		ingameButton.GetComponent<PrefabUniqueID>().saveAndLoad = this;
		return gameObject;
	}

	public static bool CanLoadSaveFile() {
		// check file exists
		string path = Application.persistentDataPath + "/save.json";
		// if file does NOT exist, return false
		if (!System.IO.File.Exists(path)) {
			return false;
		}
		// open file for reading
		StreamReader file = new StreamReader(path);
		// read into struct
		GameObjects gameObjects = JsonUtility.FromJson<GameObjects>(file.ReadToEnd());
		// close file
		file.Close();
		// return if loaded version is compatible with current version
		if (gameObjects.versionNeededToLoad != version) {
			Debug.LogWarning("Incompatible version (" + gameObjects.versionNeededToLoad + ") tried to load");
			Debug.LogWarning("Current SaveAndLoad needs version " + version);
			return false;
		}
		return true;
	}
}
