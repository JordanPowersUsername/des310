﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabUniqueID : MonoBehaviour {
	// plants are manually saved and loaded by plant pots, this was before save and load supported saving and loading of instantiated objects.
	// instead of updating the old code, it's quicker to just create this boolean. given more time this boolean should be removed and new code added.
	[SerializeField]
	private bool manuallySavedAndLoaded = false;

	public SaveAndLoad saveAndLoad { set; get; } = null;

	[HideInInspector]
	public string prefabUniqueID = "";

	void OnDestroy() {
		if (!manuallySavedAndLoaded) {
			saveAndLoad.RemoveInstantiatedObjectFromHashSetOfInstantiatedObjectsToSaveAndLoad(gameObject);
		}
	}
}
