﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadLocalPosition : MonoBehaviour, ISaveableAndLoadable {
	public string Save() {
		return JsonUtility.ToJson(transform.localPosition);
	}
	public void Load(string json) {
		transform.localPosition = JsonUtility.FromJson<Vector3>(json);
	}
}
