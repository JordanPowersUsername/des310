﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadLockOutTime : MonoBehaviour, ISaveableAndLoadable {
	EnergyManager energyManager = null;

	[System.Serializable]
	struct Struct1 {
		public int numEnergy;
		public double timeOfUnlock;
	}

	public void Awake() {
		energyManager = GetComponent<EnergyManager>();
	}
	public string Save() {
		Struct1 s = new Struct1();
		s.numEnergy = energyManager.energyNodes.Count;
		s.timeOfUnlock = energyManager.timeOfUnlock;
		return JsonUtility.ToJson(s);
	}
	public void Load(string json) {
		Struct1 s = JsonUtility.FromJson<Struct1>(json);
		energyManager.RemoveAllEnergy();
		energyManager.CreateEnergyNodes(s.numEnergy);
		energyManager.lockedOutMenu.SetActive(false);
		energyManager.LockOut();
		energyManager.timeOfUnlock = s.timeOfUnlock;
	}
}
