﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadRoom : MonoBehaviour, ISaveableAndLoadable {
	RoomManager roomManager = null;
	void Awake() {
		roomManager = GetComponent<RoomManager>();
	}
	public string Save() {
		return roomManager.room.name;
	}
	public void Load(string roomName) {
		roomManager.ChangeRoom(roomName);
	}
}
