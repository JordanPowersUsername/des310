﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadStats : MonoBehaviour, ISaveableAndLoadable
{
	StatsManager statsManager;

	[System.Serializable]
	struct Struct1 {
		public int[] stats;
	}

	private void Awake() {
		statsManager = GetComponent<StatsManager>();
	}

	public string Save() {
		Struct1 s = new Struct1();
		s.stats = statsManager.sixCs;
		return JsonUtility.ToJson(s);
	}

	public void Load(string stats) {
		Struct1 s = JsonUtility.FromJson<Struct1>(stats);
		statsManager.sixCs = s.stats;
	}
}
