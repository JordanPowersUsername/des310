﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableIfCanNotLoadSaveFile : MonoBehaviour {
	void Awake() {
		if (!SaveAndLoad.CanLoadSaveFile()) {
			gameObject.GetComponent<Button>().interactable = false;
		}
	}
}
