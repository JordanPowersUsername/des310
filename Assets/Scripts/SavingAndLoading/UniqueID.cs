﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniqueID : MonoBehaviour {
	public SaveAndLoad saveAndLoad;

	[HideInInspector]
	public string uniqueID = "";

	[SerializeField]
	private int order = 0;

	void Awake() {
		if (uniqueID == "") {
			Debug.LogError("UniqueID can't be empty!");
			return;
		}
		saveAndLoad.AddObjectToListOfObjectsToSaveAndLoad(order, gameObject);
	}
}
