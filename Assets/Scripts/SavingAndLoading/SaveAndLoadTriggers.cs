﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadTriggers : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	FlagManager flagManager = null;
	ChatManager chatManager = null;

	[System.Serializable]
	struct Struct1 {
		public List<int> chatTriggerInt;
		public List<string> flagNames;
		public List<int> flagValues;
		public string name;
	}

	private void Awake() {
		chatManager = GetComponent<ChatManager>();
	}

	public string Save() {
		Struct1 s = new Struct1();
		s.chatTriggerInt = chatManager.chatTriggerInt;
		s.name = chatManager.playerName;
		s.flagValues = new List<int>();
		s.flagNames = new List<string>();
		foreach (KeyValuePair<string, int> keyValuePair in flagManager.flags) {
			s.flagNames.Add(keyValuePair.Key);
			s.flagValues.Add(keyValuePair.Value);
		}
		return JsonUtility.ToJson(s);
	}

	public void Load(string triggers) {
		Struct1 s = JsonUtility.FromJson<Struct1>(triggers);
		for (int i = 0; i < s.flagNames.Count; i++) {
			flagManager.SetFlag(s.flagNames[i], s.flagValues[i]);
		}
		chatManager.playerName = s.name;
		for (int i = 0; i < chatManager.chatOnTap.Length; i++) {
			chatManager.chatOnTap[i].animator.SetBool("Interactive", false);
			chatManager.chatOnTap[i].animation = false;
		}
		chatManager.CheckIfQuestionMark();
	}
}
