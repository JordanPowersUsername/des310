﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadPlantPot : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	PlantPrefabArray plantPrefabArray = null;
	[SerializeField]
	PlantUI plantUI = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	FlagManager flagManager = null;

	[System.Serializable]
	struct PlantPotStruct {
		public int hydration;
		public bool hasPlant;
		public int plantPrefabIndex;
		public int stage;
		public float alarm;
		public bool dead;
	}

	PlantPot plantPot;
	Hydration hydration;
	void Awake() {
		plantPot = GetComponent<PlantPot>();
		hydration = GetComponent<Hydration>();
	}
	public string Save() {
		// get variables
		PlantPotStruct plantPotStruct = new PlantPotStruct();
		plantPotStruct.hydration = hydration.Water;
		plantPotStruct.hasPlant = plantPot.plant != null;
		if (plantPotStruct.hasPlant) {
			PlantGrow plantGrow = plantPot.plant.GetComponent<PlantGrow>();
			plantPotStruct.plantPrefabIndex = plantGrow.GetPlantPrefabIndex();
			plantPotStruct.stage = plantGrow.stage;
			plantPotStruct.alarm = plantGrow.alarm;
			plantPotStruct.dead = plantGrow.dead;
		}

		return JsonUtility.ToJson(plantPotStruct);
	}
	public void Load(string json) {
		PlantPotStruct plantPotStruct = JsonUtility.FromJson<PlantPotStruct>(json);
		hydration.Water = plantPotStruct.hydration;
		// if there is already a plant in the plant pot, destroy it
		Destroy(plantPot.plant);
		// if we are meant to have plant a plant, create one
		if (plantPotStruct.hasPlant) {
			// instantiate plant and set plant pot's plant to the new plant
			GameObject plantPrefab = plantPrefabArray.GetPlant(plantPotStruct.plantPrefabIndex);
			GameObject newPlant = Instantiate(plantPrefab, transform);
			plantPot.plant = newPlant;
			// set variables
			PlantGrow plantGrow = newPlant.GetComponent<PlantGrow>();
			plantGrow.stage = plantPotStruct.stage;
			plantGrow.alarm = plantPotStruct.alarm;
			plantGrow.dead = plantPotStruct.dead;
			plantGrow.plantUI = plantUI;
			plantGrow.gameStateManager = gameStateManager;
			plantGrow.flagManager = flagManager;
			plantGrow.UpdateSprite();
			flagManager.SetFlag("Plant", 0);
		}
	}
}
