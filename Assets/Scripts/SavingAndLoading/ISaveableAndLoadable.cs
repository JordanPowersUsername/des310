﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveableAndLoadable {
	string Save();
	void Load(string json);
}