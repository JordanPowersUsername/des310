﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadActive : MonoBehaviour, ISaveableAndLoadable {
	[System.Serializable]
	struct Active {
		public bool active;
	}
	public string Save() {
		Active active;
		active.active = gameObject.activeSelf;
		return JsonUtility.ToJson(active);
	}
	public void Load(string json) {
		Active active = JsonUtility.FromJson<Active>(json);
		if (!active.active) {
			gameObject.SetActive(false);
		}
	}
}
