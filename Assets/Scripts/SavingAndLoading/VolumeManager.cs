﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour {
	[SerializeField]
	Slider volumeSlider = null;
	[SerializeField]
	Slider musicVolumeSlider = null;

	// when setting volume, save to player preferences
	public void SetVolume(float newVolume) {
		AudioListener.volume = newVolume;
		PlayerPrefs.SetFloat("Volume", newVolume);
	}
	float musicVolume;
	public void SetMusicVolume(float newVolume) {
		musicVolume = newVolume;
		if (state == State.Constant) {
			audioSource.volume = musicVolume;
		}
		foreach (AudioSource externalMusicAudioSource in extenalMusicAudioSources) {
			externalMusicAudioSource.volume = musicVolume;
		}
		PlayerPrefs.SetFloat("MusicVolume", musicVolume);
	}
	// apon awaking, get volume from player preferences and set volume slider if it exists
	AudioSource audioSource;
	[SerializeField]
	AudioSource[] extenalMusicAudioSources = null;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
		AudioListener.volume = PlayerPrefs.GetFloat("Volume", 1f);
		if (volumeSlider) {
			volumeSlider.value = AudioListener.volume;
		}
		musicVolume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
		if (musicVolumeSlider) {
			musicVolumeSlider.value = musicVolume;
		}
		foreach (AudioSource externalMusicAudioSource in extenalMusicAudioSources) {
			externalMusicAudioSource.volume = musicVolume;
		}
		StartFadeIn();
	}
	enum State {
		FadingIn,
		Constant,
		FadingOut,
		Muted
	}
	State state;
	float fadeInSpeed = 0.333f;
	float fadeOutSpeed = 0.333f;
	void FixedUpdate() {
		switch (state) {
			case State.FadingIn:
				audioSource.volume += Time.deltaTime * fadeInSpeed;
				if (audioSource.volume >= musicVolume) {
					audioSource.volume = musicVolume;
					state = State.Constant;
				}
				break;
			case State.FadingOut:
				audioSource.volume -= Time.deltaTime * fadeOutSpeed;
				if (audioSource.volume <= 0) {
					audioSource.volume = 0;
					state = State.Muted;
					// set music position
					PlayerPrefs.SetInt("MusicTime", audioSource.timeSamples);
				}
				break;
		}
	}
	public void StartFadeIn() {
		audioSource.timeSamples = PlayerPrefs.GetInt("MusicTime", 0);
		state = State.FadingIn;
	}
	public void StartFadeOut() {
		state = State.FadingOut;
	}
}
