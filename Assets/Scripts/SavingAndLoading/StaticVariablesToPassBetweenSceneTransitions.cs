﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this class is static, it can't be put on an object, and goes between scenes



// save/load variables are read by the ChangeScene script.

// If the boolean for saving is true, then the game will save when exiting the
// Gameplay scene.
// If the boolean for loading is true, then the game will load when entering the
// Gameplay scene.

// Saving is currently always set to true
// Loading is currently set by which button is pressed on the main menu, then
// once in game is set to true if false AFTER gameplay scene has been entered



// succeededMinigame is null when player has not won or lost a minigame
// is true when player has won a minigame
// is false when player has lost a minigame
// read before loading has occured, as loading will reset this to null
// after read, onWonMinigame, and onLostMinigame unity events are called

public static class StaticVariablesToPassBetweenSceneTransitions {
	public static bool saveOnGameplaySceneExit = true;
	public static bool loadOnGameplaySceneEnter = false;

	public static string playerName = "Carl";
	public static string playerPronouns1 = "He";
	public static string playerPronouns2 = "Him";

	public static bool? succeededMinigame = null;
}
