﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadPosition : MonoBehaviour, ISaveableAndLoadable {
	public string Save() {
		return JsonUtility.ToJson(transform.position);
	}
	public void Load(string json) {
		transform.position = JsonUtility.FromJson<Vector3>(json);
	}
}
