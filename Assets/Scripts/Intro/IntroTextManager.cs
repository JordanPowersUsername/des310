﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroTextManager : MonoBehaviour
{
	enum State {
		text,
		fadingIn,
		fadingOut,
		waiting,
		inputText,
		inputButtons,
		end
	}

	[SerializeField]
	private string[] message;
	[SerializeField]
	private float betweenTimer = 0.5f;
	[SerializeField]
	private float fadeAmount = 0.005f;
	[SerializeField]
	private InputField inputField = null;
	[SerializeField]
	private GameObject button = null;
	[SerializeField]
	private Text inputText = null;
	[SerializeField]
	private Text continueText = null;
	[SerializeField]
	private GameObject pronounButtons = null;
	[SerializeField]
	private ChangeScene changeScene = null;

	public string playerName;
	public string pronoun;

	private State state = State.fadingIn;
	private float timer = 0f;
	private int messageNum = 0;
	
	private Text text;
	private Color textColor = new Color(1f, 1f, 1f, 0f);
	
	private void Start() {
		text = GetComponentInChildren<Text>();
		text.text = message[messageNum];
		text.color = textColor;
		inputField.characterLimit = 20;
	}

	private void Update() {
		switch (state) {
		case State.text:
			if (Input.GetMouseButtonDown(0)) {
				state = State.fadingOut;
				CancelInvoke("FadeText");
				continueText.color = new Color(1f, 1f, 1f, 0f);
				continueText.enabled = false;
			}
			break;
		case State.fadingIn:
			textColor.a += fadeAmount * Time.deltaTime;
			if (textColor.a >= 1f) {
				textColor.a = 1f;
				state = State.text;
				continueText.enabled = true;
				InvokeRepeating("FadeText", 0f, 0.05f);
				if (messageNum == 7) {
					state = State.inputText;
					inputField.gameObject.SetActive(true);
					button.SetActive(true);
				}
				if (messageNum == 8) {
					pronounButtons.SetActive(true);
					state = State.inputButtons;
				}
			}
			break;
		case State.fadingOut:
			textColor.a -= fadeAmount * Time.deltaTime;
			if (textColor.a <= 0f) {
				textColor.a = 0f;
				state = State.waiting;
				messageNum += 1;
				if (messageNum < message.Length) {
					text.text = message[messageNum];
				}
				else {
					changeScene.LoadScene("GameplayScene");
					StaticVariablesToPassBetweenSceneTransitions.playerName = playerName;
					StaticVariablesToPassBetweenSceneTransitions.playerPronouns1 = pronoun;
					if (pronoun == "He") {
						StaticVariablesToPassBetweenSceneTransitions.playerPronouns2 = "Him";
					}
					else if (pronoun == "She") {
						StaticVariablesToPassBetweenSceneTransitions.playerPronouns2 = "Her";
					}
					else {
						StaticVariablesToPassBetweenSceneTransitions.playerPronouns2 = "Them";
					}
					state = State.end;
				}
			}
			break;
		case State.waiting:
			if (timer < betweenTimer) {
				timer += Time.deltaTime;
			}
			else {
				timer = 0f;
				state = State.fadingIn;
			}
			break;
		}

		text.color = textColor;
	}

	public void FinishInputText() {
		if (inputText.text.Length > 0) {
			state = State.fadingOut;
			playerName = inputText.text;
			button.SetActive(false);
			inputField.gameObject.SetActive(false);
		}
		else {
			text.text = "Can't have empty name!";
		}
	}

	private bool fadeIn;
	private float time = 0f;
	private void FadeText() {
		Color color = continueText.color;
		
		if (fadeIn) {
			time += Time.deltaTime;
			if (time >= 1f) {
				fadeIn = false;
			}
		}
		else {
			time -= Time.deltaTime;
			if (time <= 0f) {
				fadeIn = true;
			}
		}

		color.a = Mathf.Lerp(0, 1, time);

		continueText.color = color;
	}

	public void ChoosePronoun(string chosenPronoun) {
		pronoun = chosenPronoun;
		pronounButtons.SetActive(false);
		state = State.fadingOut;
	}
}
