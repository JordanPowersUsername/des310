﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteMainMusicInBar : MonoBehaviour {
	[SerializeField]
	VolumeManager volumeManager;
	// when room is entered, start fading out regular music
	void OnEnable() {
		volumeManager.StartFadeOut();
	}
	// when room is exited, start fading in regular music
	void OnDisable() {
		volumeManager.StartFadeIn();
	}
}
