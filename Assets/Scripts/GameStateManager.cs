﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class GameStateManager : MonoBehaviour {
	public enum GameState {
		Movement,
		Chat,
		AfterChat,
		MoveToTarget,
		MoveToTargetUninterruptible,
		PlayingPiano,
		LoveMinigame,
		ShowingStats,
		LockedOut,
		InInventory,
		InMenu,
		TransitionDoors,
		CharacterMovement,
		None
	}

	[System.Serializable]
	public struct Events {
		public string name;
		public UnityEvent enterEvents;
		public UnityEvent exitEvents;
	}
	
	[SerializeField]
	private	List<Events> events = null;
	
	public GameState state = GameState.Movement;
	public GameState secondChatStateForTutorialStuff = GameState.None;
	public void SetState(GameState state, GameObject target = null) {
		events[(int)this.state].exitEvents.Invoke();
		this.state = state;
		this.target = target;
		events[(int)this.state].enterEvents.Invoke();
	}
	public void SetStateInEditor(int state) {
		events[(int)this.state].exitEvents.Invoke();
		this.state = (GameState)state;
		events[(int)this.state].enterEvents.Invoke();
	}
	public GameObject target { get; private set; } = null;

	public void SetEvents() {
		List<Events> placeholder = new List<Events>();
		for (int i = 0; i < events.Count; i++) {
			placeholder.Add(events[i]);
		}
		events.Clear();
		for (int i = 0; i < Enum.GetNames(typeof(GameState)).Length; i++) {
			bool done = false;
			for (int j = 0; j < placeholder.Count; j++) {
				if (placeholder[j].name == Enum.GetName(typeof(GameState), i)) {
					events.Add(placeholder[j]);
					done = true;
					continue;
				}
			}
			if (!done) {
				Events thing;
				thing.name = Enum.GetName(typeof(GameState), i);
				thing.enterEvents = new UnityEvent();
				thing.exitEvents = new UnityEvent();
				events.Add(thing);
			}
		}
	}

	void ErrorIfTargetIsNotSet() {
		if (target == null) {
			Debug.LogError("Game State was set to a Game State that involved a target, but no target was set.");
		}
	}
}
