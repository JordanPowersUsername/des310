﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RoomManager : MonoBehaviour {

	[SerializeField]
	private UnityEvent onRoomEnter = null;

	public GameObject room { get; private set; } = null;

	private void Awake() {
		// enable all rooms (this is so that their Active functions are called)
		foreach (Transform child in transform) {
			child.gameObject.SetActive(true);
		}
	}

	private void Start() {
		room = transform.GetChild(0).gameObject;
		// enable first room
		room.SetActive(true);
		// disable all other rooms
		for (int i = 1; i < transform.childCount; ++i) {
			transform.GetChild(i).gameObject.SetActive(false);
		}
	}

	public void ChangeRoom(GameObject room) {
		// deactivate previous room
		this.room.SetActive(false);
		// activate current room
		room.SetActive(true);
		this.room = room;

		// invoke unity events
		onRoomEnter.Invoke();
	}

	public void ChangeRoom(string roomName) {
		// find room with name
		foreach (Transform child in transform) {
			if (child.name.Equals(roomName)) {
				// change room if found
				ChangeRoom(child.gameObject);
				return;
			}
		}
		Debug.LogError("Couldn't change to room " + roomName);
	}

	public Transform FindRoom(string roomName) {
		// find room with name
		foreach (Transform child in transform) {
			if (child.name.Equals(roomName)) {
				// change room if found
				return child;
			}
		}
		Debug.LogError("Couldn't find room " + roomName);
		return null;
	}
}
