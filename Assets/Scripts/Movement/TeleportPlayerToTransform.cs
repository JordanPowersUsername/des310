﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayerToTransform : MonoBehaviour {
	[SerializeField]
	RoomManager roomManager = null;
	[SerializeField]
	FlagManager flagManager = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	Animator fadeAnimator = null;
	Transform positionTransform = null;
	GameStateManager.GameState previousState;
	Animator animator;
	float timer = 1f;

	private void Start() {
		animator = GetComponent<Animator>();
	}

	public void TeleportPlayer(Transform transform_) {
		fadeAnimator.SetBool("FadeIn", false);
		fadeAnimator.SetBool("Fade", true);

		positionTransform = transform_;
		previousState = gameStateManager.state;
		gameStateManager.SetState(GameStateManager.GameState.CharacterMovement);
		timer = 1f;
	}

	public void SetTurnedDirection(int index) {
		animator.SetBool("facingforward", index == 1 || index == 4);
		GetComponent<SpriteRenderer>().flipX = index > 2;
	}

	private void Update() {
		if (fadeAnimator.GetBool("Fade")) {
			if (timer <= 0f) {
				if (!fadeAnimator.GetBool("FadeIn")) {
					fadeAnimator.SetBool("FadeIn", true);
					transform.position = positionTransform.position;
					roomManager.ChangeRoom(positionTransform.parent.gameObject);
					Vector3 cam = new Vector3();
					cam.x = positionTransform.parent.transform.position.x;
					cam.y = positionTransform.parent.transform.position.y;
					cam.z = -10f;
					Camera.main.transform.position = cam;
					timer = 1f;
				}
				else {
					fadeAnimator.SetBool("Fade", false);
					flagManager.SetFlag("TeleportRoom", 1);
					gameStateManager.SetState(previousState);
				}
			}
			else {
				timer -= Time.deltaTime;
			}
		}
	}
}
