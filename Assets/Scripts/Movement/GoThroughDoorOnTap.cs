﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// can save and load door being locked or not
public class GoThroughDoorOnTap : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	private MoveAlongPath moveAlongPath = null;
	[SerializeField]
	private GameStateManager gameStateManager = null;
	[SerializeField]
	private RoomManager roomManager = null;
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private FlagManager flagManager = null;
	[SerializeField]
	private int doorNumber = -1;
	[SerializeField]
	private GameObject connectedToDoor = null;
	[SerializeField]
	private AudioClip open = null;
	[SerializeField]
	private AudioClip close = null;

	[SerializeField]
	private bool isLocked = false;
	public void Lock() {
		isLocked = true;
	}
	public void Unlock() {
		isLocked = false;
	}

	private BoxCollider2D boxCollider2D;
	private PolygonCollider2D roomCollider;
	private AudioSource roomManagersAudioSource;
	private Vector2 targetPosition;
	private Vector2 connectedToDoorTargetPosition;

	void Awake() {
		boxCollider2D = GetComponent<BoxCollider2D>();
		roomCollider = GetComponentInChildren<PolygonCollider2D>();
		roomManagersAudioSource = roomManager.GetComponent<AudioSource>();
		// find child with the tag Pathfinding, use it as a target position to walk to first before going through the door
		foreach (Transform child in transform) {
			if (child.CompareTag("Pathfinding")) {
				targetPosition = child.position;
				break;
			}
		}
		// we also have to do this for the door we are connected to, as they might not have had Awake called yet
		foreach (Transform child in connectedToDoor.transform) {
			if (child.CompareTag("Pathfinding")) {
				connectedToDoorTargetPosition = child.position;
				break;
			}
		}
	}

	void Update() {
		// if unlocked and in movement state and player tapped on door (or room hitbox)
		if (gameStateManager.state == GameStateManager.GameState.Movement &&
			inputManager.state == InputManager.State.Tap &&
			(boxCollider2D.OverlapPoint(inputManager.worldPosition) ||
			(roomCollider != null && roomCollider.OverlapPoint(inputManager.worldPosition)))) {
			if (flagManager) {
				flagManager.SetFlag("Door", doorNumber);
			}
			inputManager.ConsumeTap();
			if (!isLocked) {
				// start moving towards this door
				moveAlongPath.PathfindAndStartPath(targetPosition);
				gameStateManager.SetState(GameStateManager.GameState.MoveToTarget, gameObject);
			}
		}
		// if moving towards door and moveAlongPath is disabled (arrived)
		else if (!moveAlongPath.enabled &&
			gameStateManager.state == GameStateManager.GameState.MoveToTarget &&
			gameStateManager.target == gameObject) {

			// manually create a Path that goes through the wall to the other door
			Stack<Vector2> path = new Stack<Vector2>();
			path.Push(connectedToDoorTargetPosition);
			// disable player collision
			moveAlongPath.GetComponentInParent<Collider2D>().enabled = false;
			// start this path
			moveAlongPath.StartPath(path);
			gameStateManager.SetState(GameStateManager.GameState.MoveToTargetUninterruptible, connectedToDoor);
			// play sound
			roomManagersAudioSource.PlayOneShot(open);
		}
		// if moving through door and moveAlongPath is disabled (arrived)
		else if (!moveAlongPath.enabled &&
			gameStateManager.state == GameStateManager.GameState.MoveToTargetUninterruptible &&
			gameStateManager.target == connectedToDoor) {
			// enable player collision
			moveAlongPath.GetComponentInParent<Collider2D>().enabled = true;
			// resume regular movement game state
			gameStateManager.SetState(GameStateManager.GameState.Movement);
			// load new room
			roomManager.ChangeRoom(connectedToDoor.transform.parent.gameObject);

			// play sound
			roomManagersAudioSource.PlayOneShot(close);
		}
	}

	struct IsLockedStruct {
		public bool isLocked;
	}
	public string Save() {
		IsLockedStruct isLockedStruct;
		isLockedStruct.isLocked = isLocked;
		return JsonUtility.ToJson(isLockedStruct);
	}
	public void Load(string json) {
		IsLockedStruct isLockedStruct = JsonUtility.FromJson<IsLockedStruct>(json);
		isLocked = isLockedStruct.isLocked;
	}
}