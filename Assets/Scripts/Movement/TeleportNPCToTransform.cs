﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TeleportNPCToTransform : MonoBehaviour
{
	GetNPCRelatedData npc = null;
	Transform positionTransform;
	SpriteRenderer image;
	int fading = 0;

	private void Awake() {
		npc = GetComponent<GetNPCRelatedData>();
		image = GetComponent<SpriteRenderer>();
	}

	public void TeleportNPCFadeOut(Transform transform_) {
		positionTransform = transform_;
		fading = 1;
	}

	public void TeleportNPCFadeIn(Transform transform_) {
		transform.parent = transform_.parent;
		transform.localPosition = transform_.localPosition;
		image.color = new Color(1f, 1f, 1f, 0f);
		fading = 2;
	}

	public void ChangeDefualtPosition(Transform transform_) {
		npc.defaultPosition = transform_.position;
		npc.defaultRoom = transform_.parent;
	}

	private void Update() {
		Color color = image.color;
		
		if (fading == 1) {
			color.a -= Time.deltaTime;
			if (color.a <= 0f) {
				color.a = 0f;
				fading = 0;
				transform.position = new Vector2(1000f, 0f);
				color.a = 1f;
				transform.parent = positionTransform.parent;
				transform.localPosition = positionTransform.position;
			}
		}

		else if (fading == 2) {
			color.a += Time.deltaTime;
			if (color.a >= 1f) {
				color.a = 1f;
				fading = 0;
			}
		}

		image.color = color;
	}
}
