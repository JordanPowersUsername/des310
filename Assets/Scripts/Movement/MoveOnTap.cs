﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnTap : MonoBehaviour {
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private GameStateManager gameState = null;

	private MoveAlongPath moveAlongPath;
	void Start() {
		moveAlongPath = GetComponent<MoveAlongPath>();
	}

	void Update() {
		// if tapped
		if (inputManager.state == InputManager.State.Tap) {
			// if moving towards target, cancel that and use regular movement state
			if (gameState.state == GameStateManager.GameState.MoveToTarget) {
				gameState.SetState(GameStateManager.GameState.Movement);
			}
			// if in regular movement state, consume the tap, pathfind and start path
			if (gameState.state == GameStateManager.GameState.Movement) {
				inputManager.ConsumeTap();
				moveAlongPath.PathfindAndStartPath(inputManager.worldPosition);
			}
		}
	}
}
