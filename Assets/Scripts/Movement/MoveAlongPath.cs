﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAlongPath : MonoBehaviour {
	[SerializeField]
	private Stack<Vector2> path = new Stack<Vector2>();
	private Vector2 target = Vector2.zero;
	[SerializeField]
	private Pathfinding pathfinding = null;
	[SerializeField]
	private float maximumSpeed = 1.0f;
	[SerializeField]
	private float acceleration = 1.0f;
	[SerializeField]
	private float deceleration = 1.0f;
	[SerializeField]
	private float distanceFromTargetToStartDecelerating = 0.25f;
	Animator animator = null;
	SpriteRenderer sprite = null;
	public bool flip = false;
	public bool back = false;

	// variables related to giving up moving if not moving
	private float secondsNeededToGiveUpMovingIfNotMoving = 0.1f;
	private float secondsNotMoving = 0f;
	// previous position is also now used to determine if walked over target
	private Vector2 previousPosition = Vector2.zero;

	private Rigidbody2D rigidBody2D;
	private AudioSource audioSource;
	void Awake() {
		rigidBody2D = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource>();
		animator = GetComponent<Animator>();
		sprite = GetComponent<SpriteRenderer>();
	}

	void FixedUpdate() {
		// if position hasn't changed since last update
		if (previousPosition == rigidBody2D.position) {
			// increment timer
			secondsNotMoving += Time.deltaTime;

			if (secondsNotMoving >= secondsNeededToGiveUpMovingIfNotMoving) {
				// you are stuck, stop moving
				Debug.LogWarning("MoveAlongPath can not move along path!");
				enabled = false;
				return;
			}
		}

		Vector2 towardsTarget = target - rigidBody2D.position;
		// if you are too close to the final target
		if (path.Count == 0 && towardsTarget.magnitude < distanceFromTargetToStartDecelerating) {
			// decelerate to rest
			if (Time.deltaTime * deceleration > rigidBody2D.velocity.magnitude) {
				rigidBody2D.velocity = Vector2.zero;
				// disable script
				enabled = false;
			}
			else {
				rigidBody2D.velocity -= Time.deltaTime * deceleration * rigidBody2D.velocity.normalized;
			}
		}
		else {
			// if close enough to node
			Vector2 deltaVector = target - rigidBody2D.position;
			Vector2 oldDeltaVector = target - previousPosition;
			bool passedTarget = (
				(oldDeltaVector.x < 0 && deltaVector.x > 0) ||
				(oldDeltaVector.x > 0 && deltaVector.x < 0) ||
				(oldDeltaVector.y < 0 && deltaVector.y > 0) ||
				(oldDeltaVector.y > 0 && deltaVector.y < 0)
			);

			if (path.Count != 0 && passedTarget) {
				// pop new target off path stack
				target = path.Pop();
				AnimatorFunc();
			}
			// accelerate
			rigidBody2D.velocity += Time.deltaTime * acceleration * towardsTarget.normalized;
			// do not go too fast
			if (rigidBody2D.velocity.magnitude > maximumSpeed) {
				rigidBody2D.velocity = maximumSpeed * towardsTarget.normalized;
			}
		}

		previousPosition = rigidBody2D.position;
	}

	private void AnimatorFunc() {
		if (animator != null) {
			// start walking animation
			animator.SetBool("walking", true);
			// if target is below this
			back = !(target.y <= transform.position.y);
			animator.SetBool("facingforward", !back);
			flip = !(target.x <= transform.position.x);
			sprite.flipX = flip;
		}
	}

	void OnDrawGizmos() {
		if (rigidBody2D) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(rigidBody2D.position, target);
		}
	}

	public void StartPath(Stack<Vector2> newPath) {
		// enable this
		enabled = true;
		// set path
		path = newPath;
		// take top position off the path stack
		target = path.Pop();
		// set velocity to zero
		rigidBody2D.velocity = Vector2.zero;
		// set previous position to current position, so that it doesn't think it has instantly crossed the first node
		previousPosition = rigidBody2D.position;
		// don't give up yet
		secondsNotMoving = 0f;

		AnimatorFunc();
	}

	public void PathfindAndStartPath(Vector2 targetPosition) {
		Stack<Vector2> path = pathfinding.Pathfind(transform.position, targetPosition);

		// if path stack is empty, then pathfinding failed
		if (path.Count == 0) {
			return;
		}
		// start the path
		StartPath(path);
	}

	void OnDisable() {
		rigidBody2D.velocity = Vector2.zero;

		if (animator != null) {
			// stop walking animation
			animator.SetBool("walking", false);
		}
	}

	public void SetMaximumSpeed(float maximumSpeed) {
		this.maximumSpeed = maximumSpeed;
	}
	public void SetAcceleration(float acceleration) {
		this.acceleration = acceleration;
	}
	public void SetDeceleration(float deceleration) {
		this.deceleration = deceleration;
	}
	public void SetDecelerationRadius(float radius) {
		this.distanceFromTargetToStartDecelerating = radius;
	}
	// used by animator to play stepping sounds
	[SerializeField]
	AudioClip leftFoot;
	[SerializeField]
	AudioClip rightFoot;
	public void PlayLeftFootSound() {
		audioSource.PlayOneShot(leftFoot);
	}
	public void PlayRightFootSound() {
		audioSource.PlayOneShot(rightFoot);
	}
}
