﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

	[SerializeField]
	private CircleCollider2D circleCollider2D = null;
	[SerializeField]
	private RoomManager roomManager = null;

	private List<Node> nodes = new List<Node>();

	private class Node {
		public Vector2 position;
		public float distance = float.MaxValue;
		public List<Node> neighbours = new List<Node>();
		public bool visited = false;
		public Node previous = null;
		public bool startOrEndNode = false;
	}

	void ConnectNodesToNeighbours() {
		// connect all nodes to their neighbours
		foreach (Node node in nodes) {
			foreach (Node potentialNeighbour in nodes) {
				// don't make yourself your own neighbour
				if (node == potentialNeighbour) {
					continue;
				}
				// if there are no walls between the nodes
				Vector2 towards = potentialNeighbour.position - node.position;
				if (!Physics2D.CircleCast(node.position, circleCollider2D.radius, towards, towards.magnitude)) {
					// connect the nodes
					node.neighbours.Add(potentialNeighbour);
				}
			}
		}
	}

	void OnDrawGizmos() {
		Color regularNode = new Color32(50, 205, 50, 255);
		Color startOrEndNode = new Color32(188, 221, 179, 255);
		foreach (Node node in nodes) {
			if (node.startOrEndNode) {
				continue;
			}
			foreach (Node neighbour in node.neighbours) {
				Gizmos.color = neighbour.startOrEndNode ? startOrEndNode : regularNode;
				Gizmos.DrawLine(node.position, neighbour.position);
			}
		}
	}

	Node GetNodeWithSmallestTentativeDistance(List<Node> unvisitedNodes) {
		if (unvisitedNodes.Count == 0) {
			return null;
		}
		Node nodeWithSmallestTentativeDistance = unvisitedNodes[0];
		for (int i = 1; i < unvisitedNodes.Count; ++i) {
			Node node = unvisitedNodes[i];
			if (node.distance < nodeWithSmallestTentativeDistance.distance) {
				nodeWithSmallestTentativeDistance = node;
			}
		}
		return nodeWithSmallestTentativeDistance;
	}

	public Stack<Vector2> Pathfind(Vector2 start, Vector2 end) {
		// use dijkstra algorithm on nodes
		nodes.Clear();
		// add end position node
		Node destinationNode = new Node();
		destinationNode.position = end;
		destinationNode.startOrEndNode = true;
		nodes.Add(destinationNode);
		// add all regular nodes (children of current room game object)
		Node node;
		foreach (Transform child in roomManager.room.transform) {
			// ignore children without pathfinding tag
			if (!child.CompareTag("Pathfinding")) {
				continue;
			}
			node = new Node();
			node.position = child.position;
			nodes.Add(node);
		}
		// add start position node
		node = new Node();
		node.position = start;
		node.distance = 0.0f;
		node.startOrEndNode = true;
		nodes.Add(node);

		ConnectNodesToNeighbours();

		// Create a set of all the unvisited nodes called the unvisited set.
		List<Node> unvisitedNodes = new List<Node>(nodes);

		Node currentNode = node;
		do {
			// For the current node, consider all of its unvisited neighbours and calculate their tentative distances through the current node. Compare the newly calculated tentative distance to the current assigned value and assign the smaller one.
			foreach (Node neighbour in currentNode.neighbours) {
				if (neighbour.visited) {
					continue;
				}
				float tentativeDistance = currentNode.distance + Vector2.Distance(currentNode.position, neighbour.position);
				if (tentativeDistance < neighbour.distance) {
					neighbour.distance = tentativeDistance;
					neighbour.previous = currentNode;
				}
			}
			// When we are done considering all of the unvisited neighbours of the current node, mark the current node as visited and remove it from the unvisited set. A visited node will never be checked again.
			currentNode.visited = true;
			unvisitedNodes.Remove(currentNode);

			currentNode = GetNodeWithSmallestTentativeDistance(unvisitedNodes);
		} while ((!destinationNode.visited) && (currentNode.distance != float.MaxValue));

		// create path
		Stack<Vector2> path = new Stack<Vector2>();
		currentNode = destinationNode;
		while (currentNode.previous != null) {
			path.Push(currentNode.position);
			currentNode = currentNode.previous;
		}
		return path;
	}
}

