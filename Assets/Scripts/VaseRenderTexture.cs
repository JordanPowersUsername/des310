﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseRenderTexture : MonoBehaviour {
	[SerializeField]
	RenderTexture renderTexture = null;
	[SerializeField]
	PickUpHolder pickUpHolder = null;

	new Camera camera;
	SpriteRenderer spriteRenderer;

	Texture2D texture2D;
	Sprite sprite;
	void Awake() {
		camera = GetComponentInChildren<Camera>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		// create a texture 2D
		texture2D = new Texture2D(renderTexture.width, renderTexture.height);
		// create a sprite using the texture 2D
		sprite = Sprite.Create(texture2D, new Rect(0f, 0f, renderTexture.width, renderTexture.height), new Vector2(0.15f, 0.5f));
		// set sprite renderer to use sprite
		spriteRenderer.sprite = sprite;

		UpdateSprite();
	}

	public void UpdateSprite() {
		// render the camera to the render texture
		camera.Render();
		// set our render texture as the active one
		RenderTexture.active = renderTexture;
		// read the active render texture to the texture 2D and apply it
		texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
		texture2D.Apply();
		// update pickUpHolder inventory images
		pickUpHolder.UpdateInventoryImagesSprites();
	}
}
