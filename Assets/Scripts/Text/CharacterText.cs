﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CharacterText : MonoBehaviour {

	[System.Serializable]
	public struct Choices {
		public string choice;
		public int NEXTID;
		public UnityEvent functions;
	}

	public enum Emotion {
		Dissapointed,
		Happy,
		Neutral,
		Upset,
		Extra
	}

	[System.Serializable]
	public struct Lines {
		public string MESSAGE;
		public int index;
		public ChatManager.NPCS portrait;
		[Tooltip("Franny ( Failure), Beth (Boundaries) and Aariv (Accountant)do not have an extra portrait")]
		public Emotion emotion;
		public bool moveTextbox;
		public Vector2 translate;
		public Vector2 scale;
		public bool noChoice;
		public bool overlay;
		public Choices[] choices;
	}

	[SerializeField]
	private InputManager input = null;
	[SerializeField]
	ChatManager chatManager = null;
	[SerializeField]
	private GameStateManager gameState = null;

	[SerializeField]
	private Image portraitOne = null;
	[SerializeField]
	private Sprite[] sprites = null;
	public Text text = null; //Used in chat manager.
	[SerializeField]
	Text choiceText = null;
	[SerializeField]
	GameObject choicesMenu = null;
	public List<Lines> lines = null;

	public int currentLine = -1; //Set in chat manager.
	int currentChoice = 0;
	public int previousLine = -1;

	private void Start() {
		DisableChoices();
	}

	// returns true if there was a choice to be made.
	public bool UpdateTextbox() {
		if (previousLine != currentLine) {
			//If in chatting state and there is a line to be said.
			if (hasDialog()) {
				if (previousLine > -1) {
					//Invoke any functions and setup the variables for the next line of text.
					lines[previousLine].choices[0].functions.Invoke();
				}

				//Set the current choice to 0. This is to stop currentChoice from going out of bounds.
				currentChoice = 0;

				//Set the portrait and the text.
				if (lines[currentLine].moveTextbox){
					transform.Translate(lines[currentLine].translate);
					transform.localScale = lines[currentLine].scale;
				}

				portraitOne.sprite = sprites[(int)lines[currentLine].portrait * 5 + (int)lines[currentLine].emotion];
				chatManager.SetText(lines[currentLine].MESSAGE);

				//If there is a choice to be made return true.
				if (!lines[currentLine].noChoice && !lines[currentLine].overlay) {
					return true;
				}
				if (previousLine != -1 && lines[previousLine].overlay) {
					currentLine = previousLine;
					currentChoice = 1;
					return false;
				}

				previousLine = currentLine;
				currentLine = lines[currentLine].choices[0].NEXTID;

				if (!lines[previousLine].noChoice) {
					return true;
				}
			}
			else {
				if (previousLine > -1) {
					//Invoke any functions and setup the variables for the next line of text.
					lines[previousLine].choices[0].functions.Invoke();
				}
				//Set the textbox text to "" which will make it dissapear and let the player move again.
				text.text = "";

				previousLine = -1;
				chatManager.AfterChat();
			}
		}
		else {
			choicesMenu.transform.SetAsLastSibling();
		}
		return false;
	}

	public void ConfirmChoice() {
		if (input.state == InputManager.State.Tap) {
			input.ConsumeTap();
			previousLine = currentLine;
			if (previousLine != -1 && lines[previousLine].overlay) {
				previousLine = 0;
			}
			currentLine = lines[currentLine].choices[currentChoice].NEXTID;
			chatManager.showButtons = false;
			DisableChoices();
			chatManager.buttons = UpdateTextbox();
		}
	}

	//Called if a up or down arrow is pressed during choice making.
	//Sets the current choice either one up or down and then makes sure it is in range.
	//Then sets the text to be the current choice.
	public void ChangeText(int i) {
		currentChoice += i;
		if (currentChoice > lines[currentLine].choices.Length - 1) {
			currentChoice = 0;
		}
		else if (currentChoice < 0) {
			currentChoice = lines[currentLine].choices.Length - 1;
		}
		if (lines[currentLine].overlay && currentChoice == 0) {
			currentChoice = 1;
		}

		choiceText.text = lines[currentLine].choices[currentChoice].choice;
		choiceText.text = choiceText.text.Replace("(name)", chatManager.playerName);
		choiceText.text = choiceText.text.Replace("(Rocky)", chatManager.rocky);
	}

	//If there is a line of text to be said.
	private bool hasDialog() {
		return (currentLine > -1);
	}

	//Show the choices menu.
	public void SetUpChoices() {
		choicesMenu.SetActive(true);
		choicesMenu.transform.SetAsLastSibling();
		if (previousLine != -1 && lines[previousLine].overlay) {
			TextboxAnimation textbox = GetComponent<TextboxAnimation>();
			textbox.transform.SetAsLastSibling();
			currentChoice++;
			choiceText.text = lines[previousLine].choices[currentChoice].choice;
		}
		else {
			if (lines[currentLine].choices.Length != 5) {
				RandomizeChoices();
			}
			choiceText.text = lines[currentLine].choices[currentChoice].choice;
			choiceText.text = choiceText.text.Replace("(name)", chatManager.playerName);
			choiceText.text = choiceText.text.Replace("(Rocky)", chatManager.rocky);
		}
	}

	private void RandomizeChoices() {
		Choices[] temp = new Choices[lines[currentLine].choices.Length];
		List<int> available = new List<int>();

		for (int i = 0; i < lines[currentLine].choices.Length; i++) {
			available.Add(i);
		}

		for (int i = 0; i < temp.Length; i++) {
			int random = Random.Range(0, available.Count);
			temp[i] = lines[currentLine].choices[available[random]];
			available.RemoveAt(random);
		}

		for (int i = 0; i < temp.Length; i++) {
			lines[currentLine].choices[i] = temp[i];
		}
	}

	//Disable the choices menu.
	private void DisableChoices() {
		choicesMenu.SetActive(false);
		choiceText.text = "";
	}

	public void SkipText() {
		bool done = false;
		if (currentLine > -1) {
			do {
				lines[currentLine].choices[0].functions.Invoke();
				previousLine = currentLine;
				currentLine = lines[currentLine].choices[0].NEXTID;
				if (currentLine == -1) {
					done = true;
					UpdateTextbox();
				}
			} while (!done);
		}
	}

	public string SetUpText(string line) {
		if (lines[currentLine].portrait == ChatManager.NPCS.SHOWMAN) {
			return line.ToUpper();
		}
		return line;
	}
}
