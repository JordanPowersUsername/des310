﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagManager : MonoBehaviour {
	[SerializeField]
	private ChatManager chatManager = null;
	public Dictionary<string, int> flags = new Dictionary<string, int>();

	private void Awake() {
		SetFlag("Seed", 0); //All of the plants in order.
		SetFlag("Plant", 0); //0 - no seed, 1- seed, 2 - deadPlant, 3 - alivePlant 
		SetFlag("Inventory", 0); //0 - nothing, 1 - justOpened, 2 - justClosed
		SetFlag("Menu", 0); //0 - nothing, 1 - justOpened, 2 - justClosed
		SetFlag("Tutorial", 0); //How far we are through the tutorial.
		SetFlag("Door", 0); //Which door was last clicked on.
		SetFlag("Stats", 0);
		SetFlag("TeleportRoom", 0);
		SetFlag("Accountant", 0);
		SetFlag("Mini", 0);
		SetFlag("Pot", 0);
		SetFlag("Gardener", 0);
		SetFlag("Haunted", 0);
		SetFlag("MachineAvailable", 0);
	}

	private void Update() {
		SetFlag("Door", 0);
		SetFlag("TeleportRoom", 0);
		SetFlag("Pot", 0);
		SetFlag("Haunted", 0);
		SetFlag("MachineAvailable", 0);
	}

	// public functions
	public void SetFlag(string key, int value) {
		if (flags.ContainsKey(key)) {
			flags[key] = value;
		}
		else {
			flags.Add(key, value);
		}
	}

	public void SetFlagStringInt(string flag) {
		int value = (int)char.GetNumericValue(flag[flag.Length - 1]);
		flag = flag.Remove(flag.Length - 1);
		SetFlag(flag, value);
	}

	public int GetFlag(string key) {
		if (flags.ContainsKey(key)) {
			return flags[key];
		}
		return -1;
	}

	public void IncrementFlag(string key) {
		if (flags.ContainsKey(key)) {
			flags[key] += 1;
		}
	}
}
