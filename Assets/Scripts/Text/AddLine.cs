﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLine : MonoBehaviour {
	[SerializeField]
	CharacterText characterText = null;

	[SerializeField]
	private CharacterText.Lines line;

	[SerializeField]
	private int lineIndex = 0;
	[SerializeField]
	private string lineMessage = null;

	public void CreateLine() {
		Mathf.Max(1, line.choices.Length);
		line.index = characterText.lines.Count;
		print("Line number of new line: " + characterText.lines.Count);
		characterText.lines.Add(line);
		line = new CharacterText.Lines();
	}

	public void CreateLineInt() {
		print("Line number of new line: " + characterText.lines.Count);
		for (int i = 0; i < characterText.lines.Count; i++) {
			for (int j = 0; j < characterText.lines[i].choices.Length; j++) {
				if (characterText.lines[i].choices[j].NEXTID >= lineIndex) {
					characterText.lines[i].choices[j].NEXTID += 1;
				}
			}
		}
		line.index = characterText.lines.Count;
		Mathf.Max(1, line.choices.Length);
		characterText.lines.Insert(lineIndex, line);
		line = new CharacterText.Lines();
	}

	public void RemoveLineInt() {
		if (lineIndex < characterText.lines.Count && lineIndex >= 0) {
			characterText.lines.RemoveAt(lineIndex);
			for (int i = 0; i < characterText.lines.Count; i++) {
				for (int j = 0; j < characterText.lines[i].choices.Length; j++) {
					if (characterText.lines[i].choices[j].NEXTID >= lineIndex) {
						characterText.lines[i].choices[j].NEXTID -= 1;
					}
				}
			}
		}
	}

	public void RemoveLineString() {
		for (int i = 0; i < characterText.lines.Count; i++) {
			if (characterText.lines[i].MESSAGE == lineMessage) {
				characterText.lines.RemoveAt(i);
				continue;
			}
		}
	}

	public void ShowLineInt() {
		if (lineIndex < characterText.lines.Count && lineIndex >= 0) {
			line = characterText.lines[lineIndex];
		}
	}

	public void ShowLineString() {
		for (int i = 0; i < characterText.lines.Count; i++) {
			if (characterText.lines[i].MESSAGE == lineMessage) {
				line = characterText.lines[i];
				continue;
			}
		}
	}

	public void ReplaceLineInt() {
		if (lineIndex < characterText.lines.Count && lineIndex >= 0) {
			characterText.lines[lineIndex] = line;
		}
		line = new CharacterText.Lines();
	}

	public void SetIndexAll() {
		for (int i = 0; i < characterText.lines.Count; i++) {
			CharacterText.Lines line_  = characterText.lines[i];
			line_.index = i;
			characterText.lines[i] = line_;
		}
	}

	public void CopyChoiceByIndex() {
		if (lineIndex < characterText.lines.Count && lineIndex >= 0) {
			line.choices = characterText.lines[lineIndex].choices;
		}
	}
}
