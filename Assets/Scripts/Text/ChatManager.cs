﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChatManager : MonoBehaviour {
	public enum Rooms {
		Hallway,
		Office,
		Elevator,
		Bedroom,
		Bar,
		Greenhouse
	}

	public enum NPCS {
		SHOWMAN,
		ACCOUNTANT,
		GARDENER,
		BOUNDARIES,
		NICKO,
		ROCKY,
		NARRATOR
	}

	[System.Serializable]
	#pragma warning disable 0649
	public struct Triggers {
		public bool tutorial;
		public int energyCost;
		public int[] textNumber;
		public int[] statsNumbers;
		public StatsManager.Stats dependentStat;
		public Rooms room;
		public NPCS npc;
		public bool repeatable;
		public bool enterRoom;
		public bool tapNPC;
		public string[] conditions;
		public int[] conditonsNum;
		public UnityEvent enterEvent;
		public UnityEvent exitEvent;
	}
	#pragma warning restore 0649

	public GetNPCRelatedData[] chatOnTap;
	private AudioSource audioSource;
	[SerializeField]
	private AudioClip[] audioClips = null;
	[SerializeField]
	private CharacterText characterText = null;
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private GameStateManager gameStateManager = null;
	[SerializeField]
	private RoomManager roomManager = null;
	[SerializeField]
	private EnergyManager energyManager = null;
	[SerializeField]
	private FlagManager flagManager = null;
	[SerializeField]
	private StatsManager statsManager = null;
	[SerializeField]
	Transform textboxTransform = null;
	[SerializeField]
	private float timerLength = 0.02f;

	public List<Triggers> chatTriggers = null;
	public List<int> chatTriggerInt = new List<int>();
	private int currentTrigger;
	[HideInInspector]
	public string playerName;
	[HideInInspector]
	public string rocky = "Rocky";

	[HideInInspector]
	public bool fadingIn = false;
	[HideInInspector]
	public string textString = "";
	[HideInInspector]
	public bool buttons = false;
	public bool showButtons { get; set; } = false;
	public bool enteredRoom { get; set; }

	void Awake() {
		audioSource = GetComponent<AudioSource>();
		CheckIfQuestionMark();
		playerName = StaticVariablesToPassBetweenSceneTransitions.playerName;

		for (int i = 0; i < chatTriggers.Count; i++) {
			chatTriggerInt.Add(i);
		}
	}

	private void Update() {
		if (characterText.previousLine == -1 && characterText.currentLine == -1
			&& gameStateManager.state != GameStateManager.GameState.TransitionDoors) {
			//For the number of conversations that can be had.
			for (int i = 0; i < chatTriggerInt.Count; i++) {
				//If the triggers to have a piece of conversation is reached.
				if (CheckRooms(chatTriggers[chatTriggerInt[i]]) && CheckConditions(chatTriggers[chatTriggerInt[i]]) && !chatTriggers[chatTriggerInt[i]].tapNPC && energyManager.TryUseUpEnergy(chatTriggers[chatTriggerInt[i]].energyCost)) {
					currentTrigger = chatTriggerInt[i];
					StartChatting();
					break;
				}
			}
		}
	}

	public void Chatting() {
		if ((gameStateManager.state == GameStateManager.GameState.Chat ||
			gameStateManager.secondChatStateForTutorialStuff == GameStateManager.GameState.Chat) && 
			(!showButtons || 
			(characterText.previousLine != -1 && characterText.lines[characterText.previousLine].overlay)) 
			&& !fadingIn) {
			inputManager.ConsumeTap();
			if (textString.Length > 0) {
				//Show the entirety of the text to be shown.
				characterText.text.text += textString;
				textString = "";
			}
			else {
				//If a choice is to be made.
				if (buttons) {
					//Show the choices screen.
					showButtons = true;
					characterText.text.text = "";
					characterText.SetUpChoices();
					buttons = false;
					if (characterText.previousLine != -1 && 
						characterText.lines[characterText.previousLine].overlay) {
						//Update the textbox.
						characterText.text.text = "";
						buttons = characterText.UpdateTextbox();
					}
				}
				//If a choice is not to be made or the textbox needs to overlay the
				//choice menu.
				else if (!buttons){
					//Update the textbox.
					characterText.text.text = "";
					buttons = characterText.UpdateTextbox();
				}
			}
		}
	}

	public void CallUpdateTextbox() {
		buttons = characterText.UpdateTextbox();
	}

	public void CheckCharacter(int npc) {
		for (int i = 0; i < chatTriggerInt.Count; i++) {
			if (npc == (int)chatTriggers[chatTriggerInt[i]].npc && CheckConditions(chatTriggers[chatTriggerInt[i]]) && chatTriggers[chatTriggerInt[i]].tapNPC && energyManager.TryUseUpEnergy(chatTriggers[chatTriggerInt[i]].energyCost)) {
				currentTrigger = chatTriggerInt[i];
				StartChatting();
				break;
			}
		}
	}

	private void StartChatting() {
		//Set the current line to be the conversation to be had and change the state. 
		//Remove this conversation from the list of conversations if it is not repeatable.
		for (int i = chatTriggers[currentTrigger].statsNumbers.Length - 1; i > -1; i--) {
			if (chatTriggers[currentTrigger].statsNumbers[i] <=
				statsManager.GetStats(chatTriggers[currentTrigger].dependentStat)) {
				characterText.currentLine = chatTriggers[currentTrigger].textNumber[i];
				break;
			}
		}

		if (characterText.currentLine == -1) {
			characterText.currentLine = chatTriggers[currentTrigger].textNumber[0];
		}

		InvokeRepeating("AddCharacter", 0f, timerLength);

		if (chatTriggers[currentTrigger].tutorial) {
			gameStateManager.secondChatStateForTutorialStuff = GameStateManager.GameState.Chat;
			buttons = characterText.UpdateTextbox();
			flagManager.IncrementFlag("Tutorial");
		}
		else {
			gameStateManager.SetState(GameStateManager.GameState.Chat);
		}
		textboxTransform.SetAsLastSibling();
		chatTriggers[currentTrigger].enterEvent.Invoke();
	}

	//If the piece of text is triggered by a room and the player has entered that room
	//Or this is not the trigger, return true.
	private bool CheckRooms(Triggers trigger) {
		if (!trigger.enterRoom) {
			return true;
		}
		else {
			if (roomManager.room == GameObject.Find(trigger.room.ToString()) && enteredRoom) {
				enteredRoom = false;
				return true;
			}
			return false;
		}
	}

	private bool CheckConditions(Triggers trigger) {
		for (int j = 0; j < trigger.conditions.Length; j++) {
			if (flagManager.GetFlag(trigger.conditions[j]) !=
				trigger.conditonsNum[j]) {
				return false;
			}
		}
		return true;
	}

	public void SetText(string text) {
		// we need to find what characters cause a newline and place in the newlines manually.
		// this is done so that when the individual characters are added for the animation, it places a newline BEFORE it realises that the word it is writing does not fit.

		//Replace placeholder words and set up the style of the text.
		text = text.Replace("(name)", playerName);
		text = text.Replace("(Rocky)", rocky);
		text = characterText.SetUpText(text);

		// create a text generator with same settings as current text generator
		TextGenerationSettings settings = characterText.text.GetGenerationSettings(characterText.text.GetPixelAdjustedRect().size);
		TextGenerator generator = new TextGenerator();
		// populate the text generator
		generator.Populate(text, settings);
		// set the text to empty
		textString = "";
		// go through each line
		for (int i = 0; i < generator.lineCount; ++i) {
			// get start of line character index
			int startOfLineCharacterIndex = generator.lines[i].startCharIdx;
			// get end of line character index (last character if on last line, first character of new line if not on last line)
			int endOfLineCharacterIndex = generator.lineCount - 1 == i ? generator.characterCount - 1 : generator.lines[i + 1].startCharIdx;
			// use end - start to calculate length
			int length = endOfLineCharacterIndex - startOfLineCharacterIndex;
			// add line to text string
			textString += text.Substring(startOfLineCharacterIndex, length);
			// add new line to text string
			textString += '\n';
		}
	}

	//Add characters to the text box one by one.
	private void AddCharacter() {
		if (textString.Length > 0) {
			characterText.text.text += textString[0];
			textString = textString.Remove(0, 1);
			// play random sound
			audioSource.PlayOneShot(audioClips[Random.Range(0, audioClips.Length)]);
		}
	}

	public void AfterChat() {
		CancelInvoke("AddCharacter");

		chatOnTap[(int)chatTriggers[currentTrigger].npc].animator.SetBool("Interactive", false);
		chatOnTap[(int)chatTriggers[currentTrigger].npc].animation = false;

		if (gameStateManager.secondChatStateForTutorialStuff == 
			GameStateManager.GameState.None) {
			gameStateManager.SetState(GameStateManager.GameState.Movement);
		}
		chatTriggers[currentTrigger].exitEvent.Invoke();
		gameStateManager.secondChatStateForTutorialStuff = GameStateManager.GameState.None;
		if (!chatTriggers[currentTrigger].repeatable) {
			chatTriggerInt.Remove(currentTrigger);
		}

		characterText.currentLine = -1;
		characterText.previousLine = -1;
		currentTrigger = -1;
		CheckIfQuestionMark();
		energyManager.LockOut();
	}

	public void CheckIfQuestionMark() {
		List<int> npcs = new List<int>();
		for (int i = 0; i < chatOnTap.Length; i++) {
			//If a question mark is not enabled, add it to the list of npc's which could potentionally chat.
			if (!chatOnTap[i].animator.GetBool("Interactive")) {
				npcs.Add(i);
			}
		}
		//For the number of triggers left in the list.
		for (int i = 0; i < chatTriggerInt.Count; i++) {
			//If npc is 0 stop cause there is no question marks left to activate.
			if (npcs.Count == 0) {
				continue;
			}
			//For every npc still in the vector.
			for (int j = 0; j < npcs.Count; j++) {
				//If the trigger calls for that npc to speak when tapped on, set their question mark on and remove them from list.
				if (npcs[j] == (int)chatTriggers[chatTriggerInt[i]].npc && chatTriggers[chatTriggerInt[i]].tapNPC
					&& CheckConditions(chatTriggers[chatTriggerInt[i]])) {
					chatOnTap[j + (3 - npcs.Count)].animator.SetBool("Interactive", true);
					chatOnTap[j + (3 - npcs.Count)].animation = true;
					npcs.RemoveAt(j);
					j--;
					
				}
			}
		}
	}
}

