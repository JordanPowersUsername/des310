﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextboxAnimation : MonoBehaviour {
	private Text text;
	private RectTransform rectTransform;
	private Image textbox;
	private Vector2 textboxMaximumSize;
	private Vector2 textboxMinimumSize;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	ChatManager chatManager = null;
	[SerializeField]
	Vector2 defaultPosition;
	CharacterText characterText = null;


	void Awake() {
		text = GetComponentInChildren<Text>();
		rectTransform = GetComponent<RectTransform>();
		textbox = GetComponent<Image>();
		characterText = GetComponent<CharacterText>();

		// Calculates the maximum and minimum size of the textbox and then sets the textbox to be the minimum size.
		textboxMaximumSize = rectTransform.localScale;
		textboxMinimumSize = new Vector2(textboxMaximumSize.x, 0);
		rectTransform.localScale = textboxMinimumSize;
	}

	private Vector2 TextboxTransform(RectTransform transform, Vector2 size) {
		return Vector2.Lerp(transform.localScale, size, 10 * Time.deltaTime);
	}

	void Update() {
		if ((gameStateManager.state != GameStateManager.GameState.Chat
			&& gameStateManager.secondChatStateForTutorialStuff != GameStateManager.GameState.Chat)
			|| chatManager.fadingIn) {
			// Closes the textbox if there is no text in it.
			rectTransform.localScale = TextboxTransform(rectTransform, textboxMinimumSize);
			if (rectTransform.localScale.y <= 0.01f) {
				rectTransform.localScale = textboxMinimumSize;
				textbox.enabled = false; //This is needed to stop the textbox from still appearing as a line in the middle of the screen.
			}
		}
		else if (characterText.previousLine == -1 || !characterText.lines[characterText.previousLine].moveTextbox){
			//Opens the text box if there is text to be displayed.
			rectTransform.localPosition = defaultPosition;
			rectTransform.localScale = TextboxTransform(rectTransform, textboxMaximumSize);
			textbox.enabled = true;
		}
	}
}
