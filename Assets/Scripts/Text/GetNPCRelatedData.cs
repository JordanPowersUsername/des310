﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetNPCRelatedData : MonoBehaviour {
	[SerializeField]
	private GameObject roomManager = null;

	public Transform defaultRoom;
	public Vector2 defaultPosition = new Vector2();
	public SpriteRenderer spriteRenderer;
	public Animator animator;
	public bool animation = false;

	private void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void MoveNPC(Vector2 position, ChatManager.Rooms room) {
		transform.SetParent(roomManager.transform.Find(room.ToString()).transform);
		transform.localPosition = position;
	}

	public void ResetNPC() {
		transform.parent = defaultRoom;
		transform.localPosition = defaultPosition;
	}

	private void OnEnable() {
		animator.SetBool("Interactive", animation);
	}
}
