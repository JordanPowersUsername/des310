﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideChildrenIfNotViewingObject : MonoBehaviour {
	[SerializeField]
	private ViewObject viewObject = null;

	bool childrenActive = true;

	void FixedUpdate() {
		if (childrenActive) {
			if (!viewObject.viewingAnObject) {
				childrenActive = false;
				for (int i = 0; i < transform.childCount; ++i) {
					transform.GetChild(i).gameObject.SetActive(childrenActive);
				}
			}
		}
		else {
			if (viewObject.viewingAnObject) {
				childrenActive = true;
				for (int i = 0; i < transform.childCount; ++i) {
					transform.GetChild(i).gameObject.SetActive(childrenActive);
				}
			}
		}
	}
}
