﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Liquid : MonoBehaviour {
	[SerializeField]
	static private int baseMilliliters = 100;
	public string medicine { get; set; } = "";
	public int millilitersCapacity { get; set; } = 0;
	public int milliliters { get; set; } = 0;
	public int antecedent { get; set; } = 0;
	public int consequent { get; set; } = 0;

	public void SetHeightBasedOnMillilitersRelativeCapacity() {
		transform.localScale = new Vector3(1f, (float)milliliters/millilitersCapacity, 1f);
	}

	static public float getScaleNeededToContainMilligrams(int milligrams, int antecedent, int consequent) {
		int milliliters = getMillilitersToContainMilligrams(milligrams, antecedent, consequent);
		return getScaleNeededToContainMilliliters(milliliters);
	}

	static public float getScaleNeededToContainMilliliters(int milliliters) {
		// calculate what scale (with same XYZ) would be needed to contain milliliters
		return Mathf.Pow((float)milliliters / baseMilliliters, 1f/3f);
	}

	static public int getMillilitersToContainMilligrams(int milligrams, int antecedent, int consequent) {
		return consequent * milligrams / antecedent;
	}

	static public float getAntecedentToContainMilligramsInBaseMilliliters(int milligrams, int consequent) {
		return (float)consequent * milligrams / baseMilliliters;
	}
}
