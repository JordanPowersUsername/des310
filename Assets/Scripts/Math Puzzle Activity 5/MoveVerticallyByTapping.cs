﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveVerticallyByTapping : MonoBehaviour {
	[SerializeField]
	private InputManager input = null;

	[SerializeField]
	private float speed = 10f;
	[SerializeField]
	private float lowestPosition = 64f;
	[SerializeField]
	private float highestPosition = 512f;

	[SerializeField]
	private AudioClip audioClip = null;

	private bool toggled = true;

	private BoxCollider2D boxCollider2D = null;
	private RectTransform rectTransform = null;
	private AudioSource audioSource = null;
	void Awake() {
		boxCollider2D = GetComponent<BoxCollider2D>();
		rectTransform = GetComponent<RectTransform>();
		audioSource = GetComponent<AudioSource>();
	}
	void Update() {
		// if tapping this
		if (input.state == InputManager.State.Tap && boxCollider2D.OverlapPoint(input.screenPosition)) {
			// toggle
			toggled = !toggled;
			// consume tap
			input.ConsumeTap();
			// play sound
			audioSource.PlayOneShot(audioClip);
		}
		// depending on toggle, either move up or down
		Vector3 position = rectTransform.anchoredPosition;
		position.y = Mathf.Lerp(position.y, toggled ? highestPosition : lowestPosition, speed * Time.deltaTime);
		rectTransform.anchoredPosition = position;
	}
}
