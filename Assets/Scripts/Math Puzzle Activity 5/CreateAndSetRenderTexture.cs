﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateAndSetRenderTexture : MonoBehaviour {
	[SerializeField]
	private string tradeName = "";
	[SerializeField]
	private string dosageStrengthPrefix = "";
	[SerializeField]
	private string quantityPrefix = "";


	private RenderTexture renderTexture;
	private Liquid liquid;

	void Awake() {
		// get camera, textMesh and liquid
		Camera camera = GetComponent<Camera>();
		liquid = GetComponentInChildren<Liquid>();

		// create Render Texture that text will be rendered to
		renderTexture = new RenderTexture(2048, 2048, 0);
		renderTexture.Create();
		// set camera to render to the render texture
		camera.targetTexture = renderTexture;
	}

	public void updateTextAndRender() {
		// set material of this object's mesh renderer to the render texture
		MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		meshRenderer.material.mainTexture = renderTexture;
		Camera camera = GetComponent<Camera>();
		TextMesh textMesh = GetComponentInChildren<TextMesh>();
		// set text that will be rendered by the camera to the render texture
		textMesh.text = tradeName + "\n" + liquid.medicine + "\n" + dosageStrengthPrefix + "\n" + liquid.antecedent + "mg/" + liquid.consequent + "ml\n" + quantityPrefix + liquid.milliliters + "ml";
		// enable children related to render texture
		transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
		transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = true;
		// render and disable camera
		camera.Render();
		// disable children related to render texture
		transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
		transform.GetChild(2).GetComponent<SpriteRenderer>().enabled = false;
	}
}