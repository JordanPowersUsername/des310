﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dosages : MonoBehaviour {
	[SerializeField]
	private GameObject[] bottles = null;
	[SerializeField]
	private string[] possibleMedicineNames = null;
	[SerializeField]
	private int[] possibleDosagesInMilligrams = null;
	[SerializeField]
	private int[] possibleRatioConsequents = null;
	[SerializeField]
	private int[] possibleMilligramToXMillilitersRatiosInAscendingOrder = null;
	[SerializeField]
	private int[] possibleMillilitresInABottleUnderLessThanOrGreaterTo100InAscendingOrder = null;

	public int numberOfDoctorsOrders = 3;

	public struct DoctorsOrder {
		public int dosage;
		public string medicine;
		public int dosageInMilliliters; // answer
	}

	public List<DoctorsOrder> doctorsOrders { get; private set; } = new List<DoctorsOrder>();

	[System.Serializable]
	private struct MeshAndMaterial {
		public Mesh mesh;
		public Material material;
		public Vector3 textPosition;
		public int fontSize;
		public Sprite bgSprite;
	}
	[SerializeField]
	private MeshAndMaterial[] bottleMeshAndMaterials = null;

	enum PrintWhat {
		Nothing,
		AllInformation,
		DoctorsOrders,
		Answers
	}
	PrintWhat printWhat = PrintWhat.Answers;

	private void Awake() {
		SetRandomDosages();
	}

	public void SetRandomDosages() {
		List<string> availableMedicineNames = new List<string>(possibleMedicineNames);
		List<GameObject> availableBottles = new List<GameObject>(bottles);
		doctorsOrders.Clear();
		// create doctors orders and other random bottles
		for (int i = 0; i < bottles.Length; ++i) {
			// choose a random medicine name and remove it from the list
			int randomIndex = Random.Range(0, availableMedicineNames.Count);
			string medicine = availableMedicineNames[randomIndex];
			availableMedicineNames.RemoveAt(randomIndex);
			// choose a random dosage
			randomIndex = Random.Range(0, possibleDosagesInMilligrams.Length);
			int dosage = possibleDosagesInMilligrams[randomIndex];
			// choose a random bottle and remove it from the list
			randomIndex = Random.Range(0, availableBottles.Count);
			GameObject bottle = availableBottles[randomIndex];
			availableBottles.RemoveAt(randomIndex);
			// choose a random ratio consequent
			randomIndex = Random.Range(0, possibleRatioConsequents.Length);
			int randomConsequent = possibleRatioConsequents[randomIndex];
			// get minimum ratio bottle could hold
			int minimumMilligramToXMillilitersRatio = Mathf.CeilToInt(Liquid.getAntecedentToContainMilligramsInBaseMilliliters(dosage, randomConsequent));
			// find earliest index in ratios above this minimum ratio
			int earliestIndex = 0;
			while (possibleMilligramToXMillilitersRatiosInAscendingOrder[earliestIndex] < minimumMilligramToXMillilitersRatio) {
				++earliestIndex;
			}
			// remove ratios that don't create an integer quotient (since we will be wanting the player to calculate interger ml (same as ratio, so 5ml), real is too precise)
			List<int> possibleRatiosThatCreateAnIntegerQuotient = new List<int>();
			for (int j = earliestIndex; j < possibleMilligramToXMillilitersRatiosInAscendingOrder.Length; ++j) {
				int consequent = possibleMilligramToXMillilitersRatiosInAscendingOrder[j];
				if (dosage % consequent == 0) {
					possibleRatiosThatCreateAnIntegerQuotient.Add(consequent);
				}
			}
			// choose a random bottle ratio that is possible
			randomIndex = Random.Range(0, possibleRatiosThatCreateAnIntegerQuotient.Count);
			int milligramToXMillilitersRatio = possibleRatiosThatCreateAnIntegerQuotient[randomIndex];
			// get the minimum bottle scale that could hold the chosen milligrams with the chosen ratio
			float minimumScale = Liquid.getScaleNeededToContainMilligrams(dosage, milligramToXMillilitersRatio, randomConsequent);
			// find the earliest index in possible millilitres in a bottle
			earliestIndex = 0;
			while (Liquid.getScaleNeededToContainMilliliters(possibleMillilitresInABottleUnderLessThanOrGreaterTo100InAscendingOrder[earliestIndex]) < minimumScale) {
				++earliestIndex;
			}
			// get random bottle size that can contain the ML we need
			randomIndex = Random.Range(earliestIndex, possibleMillilitresInABottleUnderLessThanOrGreaterTo100InAscendingOrder.Length);
			int randomBottleML = possibleMillilitresInABottleUnderLessThanOrGreaterTo100InAscendingOrder[randomIndex];
			// set liquid ratio to our ratio
			Liquid liquid = bottle.GetComponentInChildren<Liquid>();
			liquid.antecedent = milligramToXMillilitersRatio;
			liquid.consequent = randomConsequent;
			// set liquid ml to our random ml
			liquid.millilitersCapacity = liquid.milliliters = randomBottleML;
			// set liquid medicine to our medicine
			liquid.medicine = medicine;
			// set bottle mesh and material to random
			randomIndex = Random.Range(0, bottleMeshAndMaterials.Length);
			bottle.GetComponent<MeshFilter>().mesh = bottleMeshAndMaterials[randomIndex].mesh;
			bottle.GetComponent<MeshRenderer>().material = bottleMeshAndMaterials[randomIndex].material;
			bottle.GetComponentInChildren<TextMesh>().fontSize = bottleMeshAndMaterials[randomIndex].fontSize;
			bottle.transform.GetChild(0).transform.localPosition = bottleMeshAndMaterials[randomIndex].textPosition;
			bottle.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = bottleMeshAndMaterials[randomIndex].bgSprite;
			// update bottle label
			CreateAndSetRenderTexture labelRenderTextureSetter = bottle.GetComponentInChildren<CreateAndSetRenderTexture>();
			labelRenderTextureSetter.updateTextAndRender();
			// set bottle size
			float scale = Liquid.getScaleNeededToContainMilliliters(randomBottleML);
			bottle.transform.localScale = Vector3.one * scale;
			// if this is one of the bottles that is meant to be a doctors oder
			if (i < numberOfDoctorsOrders) {
				// create a new doctor's order
				DoctorsOrder doctorsOrder = new DoctorsOrder();
				// set it's variables
				doctorsOrder.medicine = medicine;
				doctorsOrder.dosage = dosage;
				doctorsOrder.dosageInMilliliters = Liquid.getMillilitersToContainMilligrams(dosage, milligramToXMillilitersRatio, randomConsequent);
				// add the doctors order to the list of doctors orders
				doctorsOrders.Add(doctorsOrder);
				// print doctor's order if set to
				if (printWhat == PrintWhat.DoctorsOrders) {
					print("Doctors Order #" + (i + 1) + ":");
					print("medicine: " + doctorsOrder.medicine);
					print("mg:       " + doctorsOrder.dosage);
					print("--------");
				}
				// print answers
				if (printWhat == PrintWhat.Answers) {
					print("Doctors Order #" + (i + 1) + ":");
					print("ml: " + doctorsOrder.dosageInMilliliters);
				}
			}
			// print information
			if (printWhat == PrintWhat.AllInformation) {
				print("medicine:   " + medicine);
				print("mg:         " + dosage);
				print("consequent: " + randomConsequent);
				print("ratio:      " + milligramToXMillilitersRatio);
				print("ml needed:  " + Liquid.getMillilitersToContainMilligrams(dosage, milligramToXMillilitersRatio, randomConsequent));
				print("bottle set to " + scale + " with " + randomBottleML + " ml of medicine");
				print("--------");
			}
		}
	}
}