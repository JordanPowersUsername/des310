﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DosageTaken : MonoBehaviour {
	[SerializeField]
	ViewObject viewObject = null;

	[SerializeField]
	AudioClip drop = null;

	public Dictionary<string, int> milliliters { get; private set; } = new Dictionary<string, int>();

	[SerializeField]
	Text prefix;
	[SerializeField]
	Text infix;
	[SerializeField]
	Text suffix;
	AudioSource audioSource;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	public void TakeFromCurrentlyViewedLiquid(int milliliters) {
		if (State.tutorial) {
			return;
		}
		// if not viewing an object return
		if (viewObject.objectToView == null) {
			return;
		}
		// get liquid in viewed object
		Liquid liquid = viewObject.objectToView.GetComponentInChildren<Liquid>();
		// don't take more milliliters than stored inside liquid
		int taking = Mathf.Min(liquid.milliliters, milliliters);
		if (taking == 0) {
			return;
		}
		// decrease milliliters stored inside liquid
		liquid.milliliters -= taking;
		liquid.SetHeightBasedOnMillilitersRelativeCapacity();
		// increase milliliters stored inside us
		if (!this.milliliters.ContainsKey(liquid.medicine)) {
			this.milliliters.Add(liquid.medicine, 0);
		}
		this.milliliters[liquid.medicine] += taking;
		// update text
		UpdateText();
		// play sound
		audioSource.pitch = 2f - liquid.milliliters / 100f;
		audioSource.PlayOneShot(drop);
	}

	public void GiveToCurrentlyViewedLiquid(int milliliters) {
		if (State.tutorial) {
			return;
		}
		// if not viewing an object return
		if (viewObject.objectToView == null) {
			return;
		}
		// get liquid in viewed object
		Liquid liquid = viewObject.objectToView.GetComponentInChildren<Liquid>();
		// if we don't have any liquid of the same medicine, return
		if (!this.milliliters.ContainsKey(liquid.medicine)) {
			return;
		}
		// don't give more milliliters than stored inside us, and more than (container should be able to store the liquid since it has to have been taken out of it in the first place)
		int giving = Mathf.Min(this.milliliters[liquid.medicine], milliliters);
		if (giving == 0) {
			return;
		}
		// increase milliliters stored inside liquid
		liquid.milliliters += giving;
		liquid.SetHeightBasedOnMillilitersRelativeCapacity();
		// decrease milliliters stored inside us
		this.milliliters[liquid.medicine] -= giving;
		// remove from dictionary if at 0
		if (this.milliliters[liquid.medicine] == 0) {
			this.milliliters.Remove(liquid.medicine);
		}
		// update text
		UpdateText();
		// play sound
		audioSource.pitch = 2f - liquid.milliliters / 100f;
		audioSource.PlayOneShot(drop);
	}

	public void ClearLiquids() {
		// does not put liquid back into bottles as bottles get randomised after this is called
		milliliters.Clear();
		// update text
		UpdateText();
	}

	void UpdateText() {
		prefix.text = infix.text = suffix.text = "";
		foreach (KeyValuePair<string, int> pair in milliliters) {
			prefix.text += pair.Key + ":\n";
			infix.text += pair.Value + "\n";
			suffix.text += "ml\n";
		}
	}
}
