﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmDosagesMPA5 : MonoBehaviour {
	[SerializeField]
	ChangeScene changeScene = null;
	[SerializeField]
	private Text result = null;
	[SerializeField]
	private Image confirmButton = null;
	[SerializeField]
	private Sprite defaultSprite = null;
	[SerializeField]
	private Sprite correctSprite = null;
	[SerializeField]
	private Sprite wrongSprite = null;
	[SerializeField]
	private DosageTaken dosageTaken = null;
	[SerializeField]
	private SetPaperTextMPA5 paperTextPrefix = null;
	[SerializeField]
	private SetPaperTextMPA5 paperTextInfix = null;
	[SerializeField]
	private SetPaperTextMPA5 paperTextSuffix = null;
	[SerializeField]
	private StopwatchTimerMPA5 stopwatchTimer = null;
	[SerializeField]
	private GameObject[] gameObjectsToDeactivateThenReactivate = null;
	[SerializeField]
	private ViewObject viewObject = null;

	[SerializeField]
	private int[] secondsForDoctorOrder = null;
	[SerializeField]
	private AudioClip correct = null;
	[SerializeField]
	private AudioClip wrong = null;
	[SerializeField]
	private float secondsUntilNextTurn = 2f;
	private float secondsUntilNextTurnTimer = 0f;

	[SerializeField]
	private int remainingFailures = 15;

	private bool wasLastWrong = false;

	public int currentDoctorsOrder = 0;

	private AudioSource audioSource = null;
	private Dosages dosages = null;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
		dosages = GetComponent<Dosages>();
	}

	void FixedUpdate() {
		if (secondsUntilNextTurnTimer > 0f) {
			// count down internal timer
			secondsUntilNextTurnTimer -= Time.deltaTime;
			// if internal timer reaches 0
			if (secondsUntilNextTurnTimer <= 0f) {
				// reset text
				confirmButton.sprite = defaultSprite;
				result.text = "";
				// if was last wrong
				if (wasLastWrong) {
					// if player has lost
					if (remainingFailures < 0) {
						// change back to gameplay scene
						StaticVariablesToPassBetweenSceneTransitions.succeededMinigame = false;
						changeScene.LoadScene("GameplayScene");
						gameObject.SetActive(false);
						return;
					}
					else {
						// clear liquid
						dosageTaken.ClearLiquids();
						// set random dosages
						dosages.SetRandomDosages();
					}
				}
				// else, if all doctors orders are done
				else if (currentDoctorsOrder == dosages.numberOfDoctorsOrders) {
					// go back to gameplay
					StaticVariablesToPassBetweenSceneTransitions.succeededMinigame = true;
					changeScene.LoadScene("GameplayScene");
					gameObject.SetActive(false);
					return;
				}
				// reactivate input and gui buttons
				foreach (GameObject gameObject in gameObjectsToDeactivateThenReactivate) {
					gameObject.SetActive(true);
				}
				// update paper text
				paperTextPrefix.UpdatePaperText(currentDoctorsOrder);
				paperTextInfix.UpdatePaperText(currentDoctorsOrder);
				paperTextSuffix.UpdatePaperText(currentDoctorsOrder);
				// set timer to the time set for the doctors order
				stopwatchTimer.SetTimer(secondsForDoctorOrder[currentDoctorsOrder]);
			}
		}
	}

	// can be forced from stopwatch
	public void Confirm(bool forced = false) {
		if (State.tutorial) {
			return;
		}
		// return if internal timer is counting down
		if (secondsUntilNextTurnTimer > 0f) {
			return;
		}
		int numberOfCorrectResults = 0;
		// first check that the dosage taken is the right number
		if (dosageTaken.milliliters.Count == currentDoctorsOrder + 1) {
			// next check that each of the dosages are the same
			for (int i = 0; i <= currentDoctorsOrder; ++i) {
				Dosages.DoctorsOrder doctorsOrder = dosages.doctorsOrders[i];
				// try get the value of the medicine
				int value;
				dosageTaken.milliliters.TryGetValue(doctorsOrder.medicine, out value);
				// if the value is the same as the dosage the doctor ordered
				if (value == doctorsOrder.dosageInMilliliters) {
					// increment the number of correct results
					++numberOfCorrectResults;
				}
			}
		}
		bool allCorrect = numberOfCorrectResults == currentDoctorsOrder + 1;

		if (allCorrect) {
			// CORRECT
			confirmButton.sprite = correctSprite;
			// play sound
			audioSource.PlayOneShot(correct);
			// mark wasLastWrong flag as false
			wasLastWrong = false;
			// move onto next doctor order
			++currentDoctorsOrder;
		}
		else {
			// if failed from stopwatch, instantly fail
			if (forced) {
				remainingFailures = 0;
			}
			--remainingFailures;
			// WRONG
			confirmButton.sprite = wrongSprite;
			if (remainingFailures < 0) {
				result.text = "Too many failures";
			}
			// play sound
			audioSource.PlayOneShot(wrong);
			// mark wasLastWrong flag as true
			wasLastWrong = true;
			// reset current doctor's order to 0
			currentDoctorsOrder = 0;
		}
		// deactivate input and gui buttons
		foreach (GameObject gameObject in gameObjectsToDeactivateThenReactivate) {
			gameObject.SetActive(false);
		}
		// stop viewing object
		viewObject.viewingAnObject = false;
		// start internal timer
		secondsUntilNextTurnTimer = secondsUntilNextTurn;
		// disable stopwatch timer
		stopwatchTimer.enabled = false;
	}
}
