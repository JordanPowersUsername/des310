﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPaperTextMPA5 : MonoBehaviour {
	enum Affix {
		Prefix,
		Infix,
		Suffix
	}
	[SerializeField]
	Affix affix;

	[SerializeField]
	private Dosages dosages = null;

	void Start() {
		UpdatePaperText(0);
	}

	public void UpdatePaperText(int currentDoctorsOrder) {
		Text text = GetComponent<Text>();
		text.text = "";
		for (int i = 0; i <= currentDoctorsOrder; ++i) {
			Dosages.DoctorsOrder doctorsOrder = dosages.doctorsOrders[i];
			switch (affix) {
				case Affix.Prefix:
					text.text += doctorsOrder.dosage + "\n";
					break;
				case Affix.Infix:
					text.text += "mg\n";
					break;
				case Affix.Suffix:
					text.text += "of " + doctorsOrder.medicine + "\n";
					break;
			}
			
		}
	}
}
