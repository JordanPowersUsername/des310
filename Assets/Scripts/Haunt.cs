﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Haunt : MonoBehaviour {
	bool haunted = false;

	SpriteRenderer spriteRenderer;
	Animator animator;
	[SerializeField]
	HauntManager hauntManager = null;
	[SerializeField]
	Transform playerTransform;
	Transform npcTransform;
	bool playedSound = false;
	AudioSource audioSource;
	public int timeToNotHaunt = 0;
	public bool haunting = false;
	void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		if (spriteRenderer.material.name != "HauntedMaterial (Instance)") {
			Debug.LogError("Material must be HauntedMaterial to use Haunt script");
		}
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
	}

	void FixedUpdate() {
		// if haunted
		if (haunted) {
			// if player is close enough to this and not played sound
			if (!playedSound) {
				if (Vector2.Distance(playerTransform.position, transform.position) < 1f) {
					// play sound
					playedSound = true;
					audioSource.Play();
				}
			}
			// if player gets far enough away, allow playing of sound again
			else {
				if (Vector2.Distance(playerTransform.position, transform.position) >= 2f) {
					playedSound = false;
				}
			}
		}
	}

	public void BeginHaunt(Transform npcTrans) {
		haunted = true;
		animator.SetBool("Haunted", haunted);
		npcTransform = npcTrans;
		npcTransform.position += new Vector3(0f, -1000f, 0f);
		timeToNotHaunt = (int)TimeManager.GetSecondsSinceEpoch() + hauntManager.lengthOfHaunting;
		hauntManager.InvokeRepeating("Haunting", 1f, 1f);
		haunting = true;
	}
	public void EndHaunt() {
		haunted = false;
		animator.SetBool("Haunted", haunted);
		npcTransform.position += new Vector3(0f, 1000f, 0f);
		haunting = false;
	}
	void OnEnable() {
		animator.SetBool("Haunted", haunted);
	}
}
