﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetEnergyViaObject : MonoBehaviour
{
	[SerializeField]
	private EnergyManager energyManager = null;
	[SerializeField]
	private int regainableEnergy = 1;
	[SerializeField]
	FlagManager flagManager = null;
	[SerializeField]
	private double lockOutTime = 10;
	private Haunt haunt;
	[SerializeField]
	private double timeOut = 10;
	private double timeAvailable = 0;
	[SerializeField]
	private int machine = 2;

	private void Awake() {
		haunt = GetComponent<Haunt>();
	}

	public void RegainEnergy() {
		if (haunt && haunt.haunting) {
			flagManager.SetFlag("Haunted", 1);
			return;
		}
		if (timeAvailable < TimeManager.GetSecondsSinceEpoch()) {
			if (energyManager.energyNodes.Count != energyManager.maxEnergy) {
				timeAvailable = timeOut + TimeManager.GetSecondsSinceEpoch();
				int newEnergy = Mathf.Min(energyManager.maxEnergy - energyManager.energyNodes.Count,
					regainableEnergy);
				energyManager.LockOutObject(regainableEnergy, lockOutTime, machine);
			}
			else {
				flagManager.SetFlag("MachineAvailable", 5);
			}
		}
		else {
			if (machine == 2) {
				flagManager.SetFlag("MachineAvailable", 1);
			}
			else {
				flagManager.SetFlag("MachineAvailable", 4);
			}
		}
	}
}
