﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyManager : MonoBehaviour
{
	[SerializeField]
	GameObject energyNode_ = null;
	[SerializeField]
	private GameStateManager gameStateManager = null;
	public GameObject lockedOutMenu = null;
	public int maxEnergy = 10;
	[SerializeField]
	float lockOutTimeInSeconds = 60f;
	[SerializeField]
	Text text = null;
	public double timeOfUnlock;
	public List<GameObject> energyNodes = new List<GameObject>();
	private int energyNodesToBeRecovered = 0;
	[SerializeField]
	FlagManager flagManager = null;
	private int machineNum = 0;

	private void Start() {
		lockedOutMenu.SetActive(false);
		CreateEnergyNodes(maxEnergy);
	}

	public bool TryUseUpEnergy(int energySpent) {
		if (energyNodes.Count - energySpent > -1) {
			for (int i = 0; i < energySpent; i++) {
				Destroy(energyNodes[energyNodes.Count - 1]);
				energyNodes.RemoveAt(energyNodes.Count - 1);
			}
			return true;
		}
		return false;
	}

	public void UseUpEnergyForEvents(int energySpent) {
		TryUseUpEnergy(energySpent);
	}

	public void LockOut() {
		if (energyNodes.Count <= 0) {
			gameStateManager.SetState(GameStateManager.GameState.LockedOut);
			lockedOutMenu.SetActive(true);
			energyNodesToBeRecovered = maxEnergy - energyNodes.Count;
			timeOfUnlock = lockOutTimeInSeconds + TimeManager.GetSecondsSinceEpoch();
			machineNum = 0;
			InvokeRepeating("UpdateText", 0f, 1f);
		}
	}

	public void LockOutObject(int num, double time, int machine) {
		gameStateManager.SetState(GameStateManager.GameState.LockedOut);
		lockedOutMenu.SetActive(true);
		energyNodesToBeRecovered = num;
		timeOfUnlock = time + TimeManager.GetSecondsSinceEpoch();
		machineNum = machine;
		InvokeRepeating("UpdateText", 0f, 1f);
	}

	private void UpdateText() {
		if ((int)TimeManager.GetSecondsSinceEpoch() >= (int)timeOfUnlock) {
			gameStateManager.SetState(GameStateManager.GameState.Movement);
			lockedOutMenu.SetActive(false);
			CreateEnergyNodes(energyNodesToBeRecovered);
			flagManager.SetFlag("MachineAvailable", machineNum);
			CancelInvoke();
		}
		else {
			double time = timeOfUnlock - TimeManager.GetSecondsSinceEpoch();
			double hours = Mathf.Floor((float)time / 60 / 60);
			double mins = Mathf.Floor((float)time / 60) - hours * 60;
			double seconds = time - (hours * 60 * 60) - (mins * 60);
			string line = "Locked Out.\nTime until unlock:\n";
			if (hours > 0) {
				line += hours.ToString("F0") + " Hours, ";
			}
			if (mins > 0) {
				line += mins.ToString("F0") + " Minutes and ";
			}
			line += seconds.ToString("F0") + " Seconds";
			text.text = line;
		}
	}

	public void CreateEnergyNodes(int numberToBeCreated) {
		if (energyNodes.Count + numberToBeCreated <= maxEnergy) {
			int numEnergy = energyNodes.Count;
			for (int i = numEnergy; i < numEnergy + numberToBeCreated; i++) {
				energyNodes.Add(Instantiate(energyNode_, transform));
				float angle = Mathf.Lerp(-1f, 180f+3f, (float)i / maxEnergy);
				Vector3 rotation = new Vector3(0f, 0f, angle);
				energyNodes[energyNodes.Count - 1].transform.Rotate(rotation);
				energyNodes[energyNodes.Count - 1].transform.Translate(new Vector3(0, 2f, 0));
			}
		}
	}

	public void RemoveAllEnergy() {
		TryUseUpEnergy(energyNodes.Count);
	}
}
