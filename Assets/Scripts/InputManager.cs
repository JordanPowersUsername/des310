﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour {
	[SerializeField]
	private CanvasScaler canvasScaler = null;

	public enum State {
		None,
		Tap,
		Hold,
		Swipe,
		Pinch
	}
	public State state { get; private set; } = State.None;

	private float pinchDistance = 0f;
	public float pinchDeltaDistance { get; private set; } = 0f;
	private float swipeDistance = 0f;
	public Vector2 swipeStart { get; private set; } = Vector2.zero;
	private float holdTime = 0f;
	public Ray touchRay { get; private set; }

	private int touchCountSinceTheLastTimeTouchCountWasZero = 0;

	// mouse input currently uses a Touch struct (so that it can have the same code as touches)
	private bool usingMouseInput;
	private Touch mouseTouch;

	[SerializeField]
	private float distanceNeededForSwipe = 20f;
	[SerializeField]
	private float elapsedTimeNeededForHold = 1f;

	public Vector2 worldPosition { get; private set; } = Vector2.zero;
	public Vector2 screenPosition { get; private set; } = Vector2.zero;

	void Awake() {
		if (Input.mousePresent) {
			usingMouseInput = true;
			mouseTouch.deltaPosition = Vector2.zero;
			mouseTouch.deltaTime = 0f;
		}
		else {
			usingMouseInput = false;
		}
		// do not pretend touches are mouse inputs
		Input.simulateMouseWithTouches = false;
	}

	void Update() {
		// treat mouse as a touch input
		int mouseTouches = 0;
		float scrollWheel = 0f;
		if (usingMouseInput) {
			mouseTouch.deltaPosition = (Vector2)Input.mousePosition - mouseTouch.position;
			mouseTouch.position = Input.mousePosition;
			if (Input.GetMouseButton(0)) {
				mouseTouches = 1;
			}
			if (Input.GetMouseButtonDown(0)) {
				mouseTouch.phase = TouchPhase.Began;
			}
			else if (Input.GetMouseButtonUp(0)) {
				mouseTouches = 1;
				mouseTouch.phase = TouchPhase.Ended;
			}
			else {
				if (mouseTouch.deltaPosition.magnitude > 0f) {
					mouseTouch.phase = TouchPhase.Moved;
				}
				else {
					mouseTouch.phase = TouchPhase.Stationary;
				}
			}
			scrollWheel = Input.GetAxis("Mouse ScrollWheel");
			if (scrollWheel != 0f) {
				mouseTouches = 2;
			}
		}

		// Handle screen touches.
		// get the touch count since the last time touch count was zero
		int touchCountIncludingMouse = usingMouseInput ? mouseTouches : Input.touchCount;
		if (touchCountIncludingMouse == 0) {
			touchCountSinceTheLastTimeTouchCountWasZero = 0;
			state = State.None;
		}
		else {
			if (touchCountSinceTheLastTimeTouchCountWasZero < touchCountIncludingMouse) {
				touchCountSinceTheLastTimeTouchCountWasZero = touchCountIncludingMouse;
			}
		}

		// one touch
		if (touchCountSinceTheLastTimeTouchCountWasZero == 1) {
			Touch touch = usingMouseInput ? mouseTouch : Input.GetTouch(0);
			// find touched screen position
			screenPosition = touch.position;
			// start touch with swipe distance at 0 and hold time of 0
			if (touch.phase == TouchPhase.Began) {
				swipeStart = screenPosition;
				swipeDistance = 0f;
				holdTime = 0f;
			}
			swipeDistance += touch.deltaPosition.magnitude;
			// check for swipe
			if (swipeDistance >= distanceNeededForSwipe) {
				state = State.Swipe;
			}
			else {
				// find touched world position
				touchRay = Camera.main.ScreenPointToRay(screenPosition);
				worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);
				// hold
				holdTime += Time.deltaTime;
				if (holdTime > elapsedTimeNeededForHold) {
					state = State.Hold;
				}
				else {
					// tap released
					if (touch.phase == TouchPhase.Ended) {
						state = State.Tap;
					}
					else {
						state = State.None;
					}
				}
			}
		}
		// two touches
		else if (touchCountSinceTheLastTimeTouchCountWasZero == 2) {
			state = State.Pinch;
			if (usingMouseInput) {
				pinchDeltaDistance = scrollWheel;
			}
			else {
				Touch touch1 = Input.GetTouch(0);
				Touch touch2 = Input.GetTouch(1);

				// if second touch has just began, find the initial distance between touches
				if (touch2.phase == TouchPhase.Began) {
					pinchDistance = Vector2.Distance(touch1.position, touch2.position);
				}
				// get the change in distance from the distance previously
				pinchDeltaDistance = Vector2.Distance(touch1.position, touch2.position) - pinchDistance;
				// update the distance
				pinchDistance = Vector2.Distance(touch1.position, touch2.position);
			}
		}
	}

	// returns a Vector2 of how many pixels relative to the Canvas Scaler's reference resolution have been swiped by since the last update
	public Vector2 GetSwipeDeltaPosition() {
		Touch touch = usingMouseInput ? mouseTouch : Input.GetTouch(0);
		// get the delta position relative to the screen size
		Vector2 rawDeltaPosition = touch.deltaPosition;
		// convert this delta position to be relative to the Canvas Scaler's reference resolution
		Vector2 screenResolution = new Vector2(Screen.width, Screen.height);
		Vector2 relativeDeltaPosition = canvasScaler.referenceResolution * rawDeltaPosition / screenResolution;
		// return relative delta position
		return relativeDeltaPosition;
	}

	public void ConsumeTap() {
		state = State.None;
	}

	// used by volume sliders in letter menu
	public void ForceSwipe() {
		swipeDistance = distanceNeededForSwipe;
		state = State.Swipe;
	}
}
