﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// EditorOnly tag can't be used for this as Piano cheat can re-activate it in non-editor builds

public class DeactivateOutsideOfEditor : MonoBehaviour {
    void Start() {
        // if not loading scene (ie new game pressed)
        if (!StaticVariablesToPassBetweenSceneTransitions.loadOnGameplaySceneEnter) {
            // deactivate outside of editor, save and load active will do the Start functionality
            if (!Application.isEditor) {
                gameObject.SetActive(false);
            }
        }
    }
}
