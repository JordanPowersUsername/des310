﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class ChangeScene : MonoBehaviour {
	[SerializeField]
	SaveAndLoad saveAndLoad = null;
	[SerializeField]
	GameStateManager gameStateManager = null;

	[SerializeField]
	RectTransform leftDoor = null;
	[SerializeField]
	RectTransform rightDoor = null;

	[SerializeField]
	UnityEvent onMinigameWin = null;
	[SerializeField]
	UnityEvent onMinigameLose = null;

	[SerializeField]
	VolumeManager volumeManager = null;

	[SerializeField]
	float secondsToTransition = 0.5f;
	float timeThatTransitionStarted = 0f;
	float timeThatTransitionShouldEnd = 0f;
	enum State {
		Closed,
		Opening,
		Open,
		Closing
	}
	State state = State.Closed;
	string sceneNameToGotoAfterClosed;

	RectTransform leftDoorsParentsRectTransform;
	RectTransform rightDoorsParentsRectTransform;
	AudioSource audioSource;
	void Awake() {
		leftDoorsParentsRectTransform  = leftDoor.parent.GetComponent<RectTransform>();
		rightDoorsParentsRectTransform = rightDoor.parent.GetComponent<RectTransform>();
		audioSource = GetComponent<AudioSource>();
	}

	public void LoadScene(string sceneName) {
		sceneNameToGotoAfterClosed = sceneName;
		leftDoor.transform.SetAsLastSibling();
		rightDoor.transform.SetAsLastSibling();
		CloseDoors();
		if (volumeManager) {
			volumeManager.StartFadeOut();
		}
	}

	void Start() {
		OpenDoors();
		if (SceneManager.GetActiveScene().name == "GameplayScene") {
			// if scene should be loaded, load scene
			if (StaticVariablesToPassBetweenSceneTransitions.loadOnGameplaySceneEnter) {
				saveAndLoad.Load();
			}
			// if scene should NOT be loaded
			else {
				// the scene should be loaded next time we enter it
				// (ie, from a minigame)
				StaticVariablesToPassBetweenSceneTransitions.loadOnGameplaySceneEnter = true;
			}

			// if minigame was won, invoke minigame win events
			if (StaticVariablesToPassBetweenSceneTransitions.succeededMinigame == true) {
				onMinigameWin.Invoke();
			}
			// if minigame was lost, invoke minigame lose events
			else if (StaticVariablesToPassBetweenSceneTransitions.succeededMinigame == false) {
				onMinigameLose.Invoke();
			}
			// reset minigame status
			StaticVariablesToPassBetweenSceneTransitions.succeededMinigame = null;
		}
	}

	void Update() {
		// if doors are closing or opening
		if (state == State.Closing || state == State.Opening) {
			// get anchored position
			Vector2 leftDoorPosition  = leftDoor.anchoredPosition;
			Vector2 rightDoorPosition = rightDoor.anchoredPosition;
			// get left, middle, and right position of screen
			float left = leftDoorsParentsRectTransform.rect.xMin;
			float middle = 0f;
			float right = rightDoorsParentsRectTransform.rect.xMax;

			// move to position during transition
			float transition = Mathf.InverseLerp(timeThatTransitionStarted, timeThatTransitionShouldEnd, Time.time);
			// invert transition if closing
			if (state == State.Closing) {
				transition = 1f - transition;
			}
			leftDoorPosition.x  = Mathf.SmoothStep(middle, left,  transition);
			rightDoorPosition.x = Mathf.SmoothStep(middle, right, transition);

			// set anchored position
			leftDoor.anchoredPosition  = leftDoorPosition;
			rightDoor.anchoredPosition = rightDoorPosition;

			// if transition has finished
			if (Time.time >= timeThatTransitionShouldEnd) {
				// set state to Open or Closed
				if (state == State.Opening) {
					state = State.Open;
					if (gameStateManager != null) {
						gameStateManager.SetState(GameStateManager.GameState.Movement);
					}
				}
				else {
					state = State.Closed;
					if (gameStateManager != null) {
						gameStateManager.SetState(GameStateManager.GameState.Movement);
					}
					// if in GameplayScene and scene should be saved, save scene
					if (SceneManager.GetActiveScene().name == "GameplayScene" &&
						StaticVariablesToPassBetweenSceneTransitions.saveOnGameplaySceneExit) {
						saveAndLoad.Save();
					}
					SceneManager.LoadScene(sceneNameToGotoAfterClosed);
				}
			}
		}
	}

	void OpenDoors() {
		timeThatTransitionStarted = Time.time;
		timeThatTransitionShouldEnd = Time.time + secondsToTransition;
		if (gameStateManager != null) {
			gameStateManager.SetState(GameStateManager.GameState.TransitionDoors);
		}
		state = State.Opening;
		audioSource.Play();
	}

	void CloseDoors() {
		timeThatTransitionStarted = Time.time;
		timeThatTransitionShouldEnd = Time.time + secondsToTransition;
		if (gameStateManager != null) {
			gameStateManager.SetState(GameStateManager.GameState.TransitionDoors);
		}
		state = State.Closing;
		audioSource.Play();
	}

	public void SetStaticLoadOnSceneEnter(bool value) {
		StaticVariablesToPassBetweenSceneTransitions.loadOnGameplaySceneEnter = value;
	}
}