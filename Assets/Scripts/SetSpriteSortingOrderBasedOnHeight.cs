﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Fake depth is NOT applied as changing the sprite size changed the collider's
// size. This was an issue when the player was moving along a path close to a
// wall. Fake depth could be reimplemented in 2 ways:
//     1) Store the sprite renderer in a child of this script's game object
//     2) Change the camera back to a perspective camera, and actually use depth
// Currently not reimplemented because of time constraints, and because the
// effect is hardly noticable, at least to me.

public class SetSpriteSortingOrderBasedOnHeight : MonoBehaviour {
	SpriteRenderer spriteRenderer;
	public float offset { set; private get; } = 0f;
	void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void FixedUpdate() {
		ForceSetSpriteSortingOrderBasedOnHeight();
	}

	public void ForceSetSpriteSortingOrderBasedOnHeight() {
		// change sorting order
		spriteRenderer.sortingOrder = -(int)((offset + transform.position.y) * 10f);
	}
}
