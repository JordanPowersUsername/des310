﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class IngameButton : MonoBehaviour {
	[SerializeField]
	private UnityEvent onTap = null;
	public IngameButtonManager ingameButtonManager = null;
	public GameStateManager gameStateManager = null;
	public MoveAlongPath moveAlongPath = null;
	[Tooltip("Higher is more important, and will take priority")]
	[SerializeField]
	private int order = 0;

	new private Collider2D collider2D;
	Transform targetTransform = null;
	void Awake() {
		collider2D = GetComponent<Collider2D>();
		// if we have a child with the tag Pathfinding, use it as a target position to walk to first before using the button
		foreach (Transform child in transform) {
			if (child.CompareTag("Pathfinding")) {
				targetTransform = child;
				break;
			}
		}
	}

	private bool startCalled = false;
	void Start() {
		// tell the ingameButtonManager that we exist
		ingameButtonManager.AddIngameButtonToSortedList(order, this);
		startCalled = true;
	}

	public bool TryInvoke(Vector2 worldPosition) {
		// if this enabled and is tapped
		if (enabled && collider2D.OverlapPoint(worldPosition)) {
			// if we have a target position
			if (targetTransform != null) {
				// set gamestate to player moving towards target
				gameStateManager.SetState(GameStateManager.GameState.MoveToTarget, gameObject);
				// move player towards our target location
				moveAlongPath.PathfindAndStartPath(targetTransform.position);
			}
			else {
				// invoke events
				onTap.Invoke();
			}
			return true;
		}
		return false;
	}

	void FixedUpdate() {
		// if we have a target position and moving towards target and moveAlongPath is disabled (arrived)
		if (targetTransform != null &&
			!moveAlongPath.enabled &&
			gameStateManager.state == GameStateManager.GameState.MoveToTarget &&
			gameStateManager.target == gameObject
		) {
			// set player back to regular movement
			gameStateManager.SetState(GameStateManager.GameState.Movement);
			// invoke events
			onTap.Invoke();
		}
	}

	void OnDestroy() {
		if (startCalled) {
			ingameButtonManager.RemoveIngameButtonFromSortedList(this);
		}
	}

	// needed because C# doesn't have a clone functionality...
	// used when instantiated in SaveAndLoad and sets variables
	public void Clone(IngameButton rhs) {
		ingameButtonManager = rhs.ingameButtonManager;
		gameStateManager    = rhs.gameStateManager;
		moveAlongPath       = rhs.moveAlongPath;
	}
}
