﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsScript : MonoBehaviour
{
	[SerializeField]
	ChangeScene changeScene = null;
	private float timer = 0f;
	private void Update() {
		Vector3 position = transform.position;
		position.y += 100f * Time.deltaTime;
		transform.position = position;
		timer += Time.deltaTime;
		if (timer > 40f && timer < 40.1f) {
			changeScene.LoadScene("MenuScene");
		}
	}
}
