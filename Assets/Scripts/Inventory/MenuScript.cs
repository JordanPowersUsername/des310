﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour
{
	[SerializeField]
	InputManager inputManager = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	FlagManager flagManager = null;
	private void Update() {
		if (inputManager.state == InputManager.State.Tap) {
			inputManager.ConsumeTap();
			gameStateManager.SetState(GameStateManager.GameState.InInventory);
			gameObject.SetActive(false);
			flagManager.SetFlag("Menu", 2);
		}
	}
}
