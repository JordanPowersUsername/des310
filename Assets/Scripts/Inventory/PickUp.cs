﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PickUp : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	PickUpHolder pickUpHolder = null;
	[SerializeField]
	[Tooltip("If True, can only be picked up in top row, if False, can only be picked up in bottom row")]
	bool topRow = false;
	// getter (used in VasePickUp)
	public bool TopRow {
		get {
			return topRow;
		}
	}
	[SerializeField]
	RoomManager roomManager = null;
	[SerializeField]
	UnityEvent onPickUp = null;
	[SerializeField]
	UnityEvent onDrop = null;

	bool pickedUp = false;
	public bool PickedUp {
		set {
			// enable or disable button on pick up or put down
			pickedUp = value;
			ingameButton.enabled = !pickedUp;
			// invoke functions
			if (pickedUp) {
				onPickUp.Invoke();
			}
			else {
				onDrop.Invoke();
			}
			// set child node to origin (else it might end up in wall)
			if (transform.childCount == 1) {
				Transform child = transform.GetChild(0);
				if (!child.CompareTag("Pathfinding")) {
					Debug.LogError("PickUp has a child that isn't a node? Why?");
					return;
				}
				child.localPosition = Vector3.zero;
			}
		}
		get {
			return pickedUp;
		}
	}

	[System.Serializable]
	struct PickUpStruct {
		public bool pickedUp;
		// if not picked up, set position and room
		public Vector3 position;
		public string room;
	}

	IngameButton ingameButton;
	void Awake() {
		ingameButton = GetComponent<IngameButton>();
	}

	void OnDestroy() {
		// need to drop because drop code reset's image and hand in held
		if (pickedUp) {
			pickUpHolder.DropPickUp(gameObject, true);
		}
	}

	public string Save() {
		PickUpStruct pickUpStruct = new PickUpStruct();
		pickUpStruct.pickedUp = PickedUp;
		if (PickedUp) {
			pickUpStruct.position = Vector3.zero;
			pickUpStruct.room = "it's not in a room silly it's picked up";
		}
		else {
			pickUpStruct.position = transform.position;
			pickUpStruct.room = transform.parent.name;
		}
		return JsonUtility.ToJson(pickUpStruct);
	}
	public void Load(string json) {
		PickUpStruct pickUpStruct = JsonUtility.FromJson<PickUpStruct>(json);
		if (pickUpStruct.pickedUp) {
			TryPickUp();
		}
		else {
			pickUpHolder.DropPickUp(gameObject, false);
			// find room
			Transform room = roomManager.FindRoom(pickUpStruct.room);
			transform.SetParent(room);
			// move
			transform.position = pickUpStruct.position;
		}
	}

	public void TryPickUp() {
		// if not already picked up
		if (!PickedUp) {
			// try pick up
			PickedUp = pickUpHolder.PickUp(gameObject, topRow);
		}
	}

	// needed because C# doesn't have a clone functionality...
	// used when instantiated in SaveAndLoad and sets variables
	public void Clone(PickUp rhs) {
		pickUpHolder = rhs.pickUpHolder;
		roomManager = rhs.roomManager;
	}
}
