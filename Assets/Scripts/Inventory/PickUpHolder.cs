﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpHolder : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	RoomManager roomManager = null;
	[SerializeField]
	GameStateManager gameStateManager = null;
	[SerializeField]
	MoveAlongPath moveAlongPath = null;
	[SerializeField]
	IngameButtonManager ingameButtonManager = null;

	private GameObject[] storage;
	[SerializeField]
	private GameObject[] inventoryImages = null;
	[SerializeField]
	private int firstIndexOfBottomRow = 7;
	[SerializeField]
	private RawImage inventoryBorder = null;

	private int? hand = null;

	[System.Serializable]
	struct PickUpInHand {
		public string handUniqueID; // can also be prefab's UniqueID
		public int size; // size of storage, needed to determine when all objects have been loaded and added
	}
	PickUpInHand loadedData;
	bool appliedLoadedData = true;

	AudioSource audioSource;
	[SerializeField]
	AudioClip setHandAudioClip = null;
	public void Awake() {
		storage = new GameObject[inventoryImages.Length];
		audioSource = GetComponent<AudioSource>();
	}

	public bool PickUp(GameObject pickUp, bool topRow) {
		// find first free slot in inventory
		int? index = null;
		int min, max;
		if (topRow) {
			min = 0; max = firstIndexOfBottomRow;
		}
		else {
			min = firstIndexOfBottomRow; max = storage.Length;
		}
		for (int i = min; i < max; i++) {
			if (!storage[i]) {
				index = i;
				break;
			}
		}
		// if there is no free slot in inventory, return false
		if (!index.HasValue) {
			return false;
		}

		// add the pickup to gameobject array
		storage[index.Value] = pickUp;
		pickUp.transform.SetParent(transform);
		// set image using sprite
		SpriteRenderer spriteRenderer = pickUp.GetComponent<SpriteRenderer>();
		Image image = inventoryImages[index.Value].GetComponent<Image>();
		image.enabled = true;
		image.sprite = spriteRenderer.sprite;
		// hide gameplay sprite
		spriteRenderer.enabled = false;

		// put the pick up in the hand
		SetHand(index.Value);

		return true;
	}

	public void SetHand(int i) {
		// if already in hand, return
		if (hand == i || !storage[i]) {
			return;
		}
		// put pickup into hand
		hand = i;

		UpdateInventoryHandBorders();

		// play sound if one isn't playing and in inventory
		if (gameStateManager.state == GameStateManager.GameState.InInventory && !audioSource.isPlaying) {
			audioSource.PlayOneShot(setHandAudioClip);
		}
	}

	public bool HasFreeSlot(bool topRow) {
		int min, max;
		if (topRow) {
			min = 0; max = firstIndexOfBottomRow;
		}
		else {
			min = firstIndexOfBottomRow; max = storage.Length;
		}
		for (int i = min; i < max; i++) {
			if (!storage[i]) {
				return true;
			}
		}
		return false;
	}

	public void DropPickUp() {
		// only work during regular movement gameplay state
		if (gameStateManager.state != GameStateManager.GameState.Movement) {
			return;
		}
		// if there is a pick up in the hand drop it
		if (hand.HasValue) {
			DropPickUp(hand.Value);
			return;
		}
	}

	public GameObject TryFindPickUpInHandWithTag(string tag) {
		// if pick up is in hand, return it
		if (hand.HasValue && storage[hand.Value].CompareTag(tag)) {
			return storage[hand.Value];
		}
		// if pick up is not in hand, return null
		return null;
	}

	public GameObject TryFindPickUpWithTag(string tag) {
		for (int i = 0; i < storage.Length; i++) {
			if (storage[i] != null && storage[i].CompareTag(tag)) {
				return storage[i];
			}
		}
		return null;
	}

	private GameObject DropPickUp(int index, bool beingDestroyed = false) {

		// hide inventory image
		if (inventoryImages[index]) {
			inventoryImages[index].GetComponent<Image>().enabled = false;
		}
		// get the PickUp to be dropped
		GameObject pickUpToBeDropped = storage[index];
		// make it's sprite renderer visible
		pickUpToBeDropped.GetComponent<SpriteRenderer>().enabled = true;
		// remove the PickUp from this storage list
		storage[index] = null;
		// if there is an PickUp in the hand
		if (hand.HasValue) {
			// if dropped pick up was in the hand, set it to null
			if (index == hand.Value) {
				hand = null;
			}
		}

		UpdateInventoryHandBorders();
		
		// set flag on pickup (pickedUp) to false so it can be picked up again
		pickUpToBeDropped.GetComponent<PickUp>().PickedUp = false;
		// if pick up is NOT being destroyed (error would occur)
		if (!beingDestroyed) {
			// set parent of pickup's transform to current room's transform
			pickUpToBeDropped.transform.localPosition = Vector2.zero;
			pickUpToBeDropped.transform.SetParent(roomManager.room.transform);
		}

		return pickUpToBeDropped;
	}

	// used when loading
	public GameObject DropPickUp(GameObject gameObject, bool beingDestroyed = false) {
		// find in list
		int index = System.Array.IndexOf(storage, gameObject); //storage.IndexOf(gameObject);
		if (index != -1) {
			// drop it
			return DropPickUp(index, beingDestroyed);
		}
		return null;
	}

	private void FixedUpdate() {
		// TrySetHandUsingLoadedID needs to be called in here, or it's called before the previous objects are destroyed and does not work
		TrySetHandUsingLoadedID();
	}

	void UpdateInventoryHandBorders() {
		// check border exists (ie, not destroyed on exit)
		if (inventoryBorder == null) {
			return;
		}
		// set inventory border enabled/disabled and position
		if (hand.HasValue) {
			inventoryBorder.enabled = true;
			inventoryBorder.transform.localPosition = inventoryImages[hand.Value].transform.localPosition;
		}
		else {
			inventoryBorder.enabled = false;
		}
	}

	public string Save() {
		PickUpInHand pickUpInHand = new PickUpInHand();
		pickUpInHand.size = storage.Length;
		// if there is a pick up in the hand
		if (hand.HasValue) {
			// try get unique id
			UniqueID uniqueID;
			bool hasUniqueID = storage[hand.Value].TryGetComponent<UniqueID>(out uniqueID);
			if (hasUniqueID) {
				pickUpInHand.handUniqueID = uniqueID.uniqueID;
			}
			// otherwise try get prefab unique id
			else {
				PrefabUniqueID prefabUniqueID;
				bool hasPrefabUniqueID = storage[hand.Value].TryGetComponent<PrefabUniqueID>(out prefabUniqueID);
				if (hasPrefabUniqueID) {
					pickUpInHand.handUniqueID = prefabUniqueID.prefabUniqueID;
				}
				else {
					Debug.LogError("Trying to save hand, but I can't identify the pick up using a UniqueID or PrefabUniqueID");
				}
			}
		}
		else {
			pickUpInHand.handUniqueID = "empty, like a lack of ability to intrepret your own feelings";
		}
		return JsonUtility.ToJson(pickUpInHand);
	}

	public void TrySetHandUsingLoadedID() {
		if (appliedLoadedData || loadedData.size != storage.Length) {
			return;
		}
		appliedLoadedData = true;
		// empty hand
		hand = null;
		// if found a UniqueID for the hand
		if (loadedData.handUniqueID.Length == 36) {
			// go through all pick up's in storage
			int index = 0;
			foreach (GameObject pickUp in storage) {
				// if there is no pick up in this slot, continue the for loop
				if (pickUp == null) {
					++index;
					continue;
				}
				// try get unique id
				UniqueID uniqueID;
				bool hasUniqueID = pickUp.TryGetComponent<UniqueID>(out uniqueID);
				if (hasUniqueID) {
					if (uniqueID.uniqueID == loadedData.handUniqueID) {
						// put in hand
						SetHand(index);
						break;
					}
				}
				else {
					// otherwise get prefab unique id
					PrefabUniqueID prefabUniqueID = pickUp.GetComponent<PrefabUniqueID>();
					if (prefabUniqueID.prefabUniqueID == loadedData.handUniqueID) {
						// put in hand
						SetHand(index);
						break;
					}
				}
				++index;
			}
			if (index == storage.Length) {
				Debug.LogError("Could NOT find saved hand id in storage");
				Debug.LogError("storage.Length: " + storage.Length);
			}
		}
	}

	public void UpdateInventoryImagesSprites() {
		// go through all inventory image's and set their sprite to their pick up's sprite
		for (int i = 0; i < storage.Length; ++i) {
			if (storage[i]) {
				inventoryImages[i].GetComponent<Image>().sprite = storage[i].GetComponent<SpriteRenderer>().sprite;
			}
		}
	}

	public void Load(string json) {
		loadedData = JsonUtility.FromJson<PickUpInHand>(json);
		appliedLoadedData = false;
	}

	public void DropEverything() {
		for (int i = 0; i < storage.Length; ++i) {
			if (storage[i]) {
				DropPickUp(i);
			}
		}
	}
}
