﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OpenInventory : MonoBehaviour {
	[SerializeField]
	private InputManager input = null;
	[SerializeField]
	private GameStateManager gameStateManager = null;
	[SerializeField]
	FlagManager flagManager = null;

	private GameStateManager.GameState previousState;

	[SerializeField]
	private float inventoryDropPosition = 100f;
	public bool isTouched { get; private set; } = false;

	new private Collider2D collider2D = null;
	private RectTransform inventorysRectTransform = null;
	private CanvasScaler canvasScaler = null;
	private AudioSource audioSource = null;
	void Start() {
		collider2D = GetComponent<Collider2D>();
		inventorysRectTransform = transform.parent.GetComponent<RectTransform>();
		canvasScaler = GetComponentInParent<CanvasScaler>();
		audioSource = GetComponent<AudioSource>();
	}
	// Update is called once per frame
	void Update() {
		if (flagManager) {
			flagManager.SetFlag("Inventory", 0); //Reset inventory flag.
			flagManager.SetFlag("Menu", 0); //Reset menu flag.
		}
		
		// get position of bottom of inventory
		Vector2 position = inventorysRectTransform.anchoredPosition;
		// use canvasScaler's height
		float height = canvasScaler.referenceResolution.y;

		if (input.state == InputManager.State.Swipe && !isTouched) {
			if (collider2D.OverlapPoint(input.swipeStart)) {
				isTouched = true;
				// go in front of inventory/ssdm because we wouldn't want to see it's rope now would we? :P
				transform.parent.SetAsLastSibling();
				if (position.y > height / 2) {
					previousState = gameStateManager.state;
				}
				gameStateManager.SetState(GameStateManager.GameState.InInventory);
				// play sound
				audioSource.Play();
			}
		}
		// if not swiping (swipe released)
		else if (input.state != InputManager.State.Swipe &&
			gameStateManager.state == GameStateManager.GameState.InInventory && isTouched) {
			// if on lower half of screen
			if (position.y < height / 2) {
				// snap position to bottom
				position.y = inventoryDropPosition;
				if (flagManager) {
					flagManager.SetFlag("Inventory", 1);
				}
			}
			// if on upper half of screen
			else {
				// snap position to top
				position.y = height;
				gameStateManager.SetState(previousState);
				if (flagManager) {
					flagManager.SetFlag("Inventory", 2);
				}
			}
			isTouched = false;
		}

		if (isTouched) {
			// move and clamp position
			position.y += input.GetSwipeDeltaPosition().y;
			position.y = Mathf.Clamp(position.y, inventoryDropPosition, height);
		}

		inventorysRectTransform.anchoredPosition = position;
	}

	public void ForceClose() {
		gameStateManager.SetState(previousState);
		if (flagManager) {
			flagManager.SetFlag("Inventory", 2);
		}
		Vector2 position = inventorysRectTransform.anchoredPosition;
		position.y = canvasScaler.referenceResolution.y;
		inventorysRectTransform.anchoredPosition = position;
		// play sound
		audioSource.Play();
		// go back to previous state
		gameStateManager.SetState(previousState);
	}
}
