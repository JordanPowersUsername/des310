﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLabelText : MonoBehaviour {
	[SerializeField]
	private string tradeName = "";
	[SerializeField]
	private string genericName = "";
	[SerializeField]
	private string dosageStrengthPrefix = "";
	[SerializeField]
	private Dosage dosage = null;
	[SerializeField]
	private string numberOfTabletsPrefix = "";
	[SerializeField]
	private TabletCount TabletCount = null;
	[SerializeField]
	private new Camera camera = null;

	void Start() {
		UpdateLabelText();
	}

	public void UpdateLabelText() {
		TextMesh textMesh = GetComponent<TextMesh>();
		textMesh.text = tradeName + "\n" + genericName + "\n" + dosageStrengthPrefix + dosage.individualDosageInMilligrams + "mg\n" + numberOfTabletsPrefix + TabletCount.getMaximumNumberOfTablets();
		camera.Render();
	}
}
