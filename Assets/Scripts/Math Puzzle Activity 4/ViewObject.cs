﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewObject : MonoBehaviour {
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private float transitionSpeed = 0.9f;

	public GameObject objectToView { private set; get; } = null;
	private Vector3 originalPosition = Vector3.zero;
	private Vector3 targetPosition = Vector3.zero;
	private Quaternion originalRotation = Quaternion.identity;
	private Quaternion targetRotation = Quaternion.identity;
	private Vector3 originalScale = Vector3.one;
	private Vector3 targetScale = Vector3.one;
	private AudioClip audioClip = null;

	public bool viewingAnObject = false;

	private AudioSource audioSource;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	private void Update() {
		if (viewingAnObject) {
			// translate their transform to the target transform
			objectToView.transform.position = Vector3.Lerp(objectToView.transform.position, targetPosition, transitionSpeed * Time.deltaTime);
			objectToView.transform.rotation = Quaternion.Lerp(objectToView.transform.rotation, targetRotation, transitionSpeed * Time.deltaTime);
			objectToView.transform.localScale = Vector3.Lerp(objectToView.transform.localScale, targetScale, transitionSpeed * Time.deltaTime);
			// if screen is tapped, consume tap and stop viewing the object
			if (inputManager.state == InputManager.State.Tap) {
				inputManager.ConsumeTap();
				viewingAnObject = false;
				audioSource.PlayOneShot(audioClip);
			}
		}
		else {
			// if we currently have an object
			if (objectToView) {
				// translate their transform back to their original transform
				objectToView.transform.position = Vector3.Lerp(objectToView.transform.position, originalPosition, transitionSpeed * Time.deltaTime);
				objectToView.transform.rotation = Quaternion.Lerp(objectToView.transform.rotation, originalRotation, transitionSpeed * Time.deltaTime);
				objectToView.transform.localScale = Vector3.Lerp(objectToView.transform.localScale, originalScale, transitionSpeed * Time.deltaTime);
			}
		}
	}

	// only set object to view if not viewing an object
	public void SetObjectToView(GameObject objectToView, Vector3 targetPosition, Quaternion targetRotation, Vector3 targetScale, AudioClip audioClip) {
		if (!viewingAnObject) {
			// if currently moving an object back to their original position
			if (this.objectToView) {
				// set it the object to their original position
				this.objectToView.transform.position = originalPosition;
				this.objectToView.transform.rotation = originalRotation;
				this.objectToView.transform.localScale = originalScale;
			}
			// update current object to view
			this.objectToView = objectToView;
			this.targetPosition = targetPosition;
			this.targetRotation = targetRotation;
			this.targetScale = targetScale;
			originalPosition = objectToView.transform.position;
			originalRotation = objectToView.transform.rotation;
			originalScale = objectToView.transform.localScale;
			viewingAnObject = true;
			// play sound
			audioSource.PlayOneShot(audioClip);
			this.audioClip = audioClip;
		}
	}
}
