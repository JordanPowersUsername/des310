﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabletCount : MonoBehaviour {
	[SerializeField]
	private GameObject tabletPrefab = null;
	[SerializeField]
	private int columns = 5;
	[SerializeField]
	private int rows = 5;

	[SerializeField]
	private AudioClip onTabletChangeAudio = null;

	[SerializeField]
	private GameObject confirmationButton = null;

	public int numberOfTablets { get; private set; } = 0;

	private AudioSource audioSource;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	public void clearTablets() {
		while (numberOfTablets > 0) {
			--numberOfTablets;
			Destroy(transform.GetChild(numberOfTablets + 1).gameObject);
		}
		confirmationButton.GetComponent<Button>().interactable = false;
	}

	public void decrementNumberOfTablets() {
		if (numberOfTablets > 0) {
			--numberOfTablets;
			Destroy(transform.GetChild(numberOfTablets + 1).gameObject);
		}
		if (numberOfTablets == 0) {
			confirmationButton.GetComponent<Button>().interactable = false;
		}
	}

	public void incrementNumberOfTablets() {
		if (numberOfTablets < columns * rows) {
			++numberOfTablets;
			Transform childTabletTransform = Instantiate(tabletPrefab, transform).transform;
			updateChildTabletTransformPosition(childTabletTransform);

			audioSource.PlayOneShot(onTabletChangeAudio);
		}
		confirmationButton.GetComponent<Button>().interactable = true;
	}

	private void updateChildTabletTransformPosition(Transform childTabletTransform) {
		Vector3 localPosition = childTabletTransform.localPosition;
		int column = (numberOfTablets - 1) % columns;
		int row = (numberOfTablets - 1) / columns;
		localPosition.x = Mathf.Lerp(-1.25f, 1.25f, column / (columns - 1f));
		localPosition.y = 2f;
		localPosition.z = Mathf.Lerp(-1.25f, 1.25f, row / (rows - 1f));
		childTabletTransform.localPosition = localPosition;
	}

	public int getMaximumNumberOfTablets() {
		return columns * rows;
	}
}
