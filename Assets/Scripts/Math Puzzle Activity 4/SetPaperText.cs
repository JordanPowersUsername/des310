﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPaperText : MonoBehaviour {

	[SerializeField]
	private Dosage dosage = null;

	[SerializeField]
	private new Camera camera = null;

	void Start() {
		UpdatePaperText();
	}

	public void UpdatePaperText() {
		TextMesh textMesh = GetComponent<TextMesh>();
		textMesh.text = dosage.randomTotalDosageInMilligrams.ToString();
		camera.Render();
	}
}
