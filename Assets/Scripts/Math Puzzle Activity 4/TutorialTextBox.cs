﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TutorialTextBox : MonoBehaviour
{
	[System.Serializable]
	struct Lines {
		public string MESSAGE;
		public Vector2 portraitPosition;
		public Vector2 portraitAnchorMinAndMax;
		public Vector2 position;
		public bool flipPortrait;
	}

	[SerializeField]
	UnityEvent endEvent;
	[SerializeField]
	private Lines[] lines;
	int currentLine = 0;
	Text text;
	[SerializeField]
	Transform portrait;

	RectTransform rectTransform;
	RectTransform portraitRectTransform;
	void Start() {
		// get rect transforms, which will be used to move the portrait and textbox
		rectTransform = GetComponent<RectTransform>();
		portraitRectTransform = portrait.GetComponent<RectTransform>();
		// get the text, which will be used to show messages
		text = GetComponentInChildren<Text>();
		// set the position to the first value in array
		rectTransform.anchoredPosition = lines[currentLine].position;
		portraitRectTransform.anchorMin = lines[currentLine].portraitAnchorMinAndMax;
		portraitRectTransform.anchorMax = lines[currentLine].portraitAnchorMinAndMax;
		portraitRectTransform.anchoredPosition = lines[currentLine].portraitPosition;
		// set the first message
		text.text = lines[currentLine].MESSAGE;
		// increment the current line
		currentLine++;
	}

	public void UpdateTextbox() {
		// if we aren't at the end of the lines we are to show
		if (currentLine < lines.Length) {
			// set the message to the next line
			text.text = lines[currentLine].MESSAGE;
			// set the positions
			rectTransform.anchoredPosition = lines[currentLine].position;
			portraitRectTransform.anchorMin = lines[currentLine].portraitAnchorMinAndMax;
			portraitRectTransform.anchorMax = lines[currentLine].portraitAnchorMinAndMax;
			portraitRectTransform.anchoredPosition = lines[currentLine].portraitPosition;
			// increment the current line
			currentLine++;
		}
		// if we are at the end of the lines we are to show
		else {
			// disable this
			transform.parent.gameObject.SetActive(false);
			// invoke end events
			endEvent.Invoke();
		}
	}
}
