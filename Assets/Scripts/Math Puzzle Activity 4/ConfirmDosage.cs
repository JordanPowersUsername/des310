﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmDosage : MonoBehaviour {
	[SerializeField]
	ChangeScene changeScene = null;
	[SerializeField]
	private Text result = null;
	[SerializeField]
	private Sprite defaultSprite = null;
	[SerializeField]
	private Sprite correctSprite = null;
	[SerializeField]
	private Sprite wrongSprite = null;
	[SerializeField]
	private TabletCount tabletCount = null;
	[SerializeField]
	private SetLabelText labelText = null;
	[SerializeField]
	private SetPaperText paperText = null;
	[SerializeField]
	private StopwatchTimer stopwatchTimer = null;
	[SerializeField]
	private GameObject[] gameObjectsToDeactivateThenReactivate = null;
	[SerializeField]
	private GameObject confirmationButton = null;
	[SerializeField]
	private Text correctInARowText = null;
	private int correctInARow = 0;
	[SerializeField]
	private int remainingFailures = 20;
	private int CorrectInARow {
		set {
			if (reachedCorrectInARowRequirement) {
				return;
			}
			correctInARow = value;
			correctInARowText.text = correctInARow.ToString() + "/" + correctInARowNeeded.ToString();
			if (correctInARow == correctInARowNeeded) {
				reachedCorrectInARowRequirement = true;
				correctInARowText.color = Color.green;
			}
		}
		get {
			return correctInARow;
		}
	}
	[SerializeField]
	private int correctInARowNeeded = 5;
	private bool reachedCorrectInARowRequirement = false;
	[SerializeField]
	private Text correctTotalText = null;
	private int correctTotal = 0;
	private int CorrectTotal {
		set {
			if (correctTotal >= correctTotalNeeded) {
				return;
			}
			correctTotal = value;
			correctTotalText.text = correctTotal.ToString() + "/" + correctTotalNeeded.ToString();
			if (correctTotal >= correctTotalNeeded) {
				correctTotalText.color = Color.green;
			}
		}
		get {
			return correctTotal;
		}
	}
	[SerializeField]
	private int correctTotalNeeded = 8;

	[SerializeField]
	private AudioClip correct = null;
	[SerializeField]
	private AudioClip wrong = null;
	[SerializeField]
	private float secondsUntilNextTurn = 2f;
	private float secondsUntilNextTurnTimer = 0f;

	private bool wasLastWrong = false;

	private AudioSource audioSource = null;
	private Dosage dosage = null;
	void Awake() {
		audioSource = GetComponent<AudioSource>();
		dosage = GetComponent<Dosage>();
	}

	void FixedUpdate() {
		if (secondsUntilNextTurnTimer > 0f) {
			// count down internal timer
			secondsUntilNextTurnTimer -= Time.deltaTime;
			// if internal timer reaches 0
			if (secondsUntilNextTurnTimer <= 0f) {
				// if both exit conditions are met
				if (reachedCorrectInARowRequirement && CorrectTotal >= correctTotalNeeded) {
					// change back to gameplay scene
					StaticVariablesToPassBetweenSceneTransitions.succeededMinigame = true;
					changeScene.LoadScene("GameplayScene");
					gameObject.SetActive(false);
					return;
				}
				// if player has lost
				if (remainingFailures < 0) {
					// change back to gameplay scene
					StaticVariablesToPassBetweenSceneTransitions.succeededMinigame = false;
					changeScene.LoadScene("GameplayScene");
					gameObject.SetActive(false);
					return;
				}
				// set random dosage
				dosage.SetRandomDosage();
				// reset text
				result.text = "";
				confirmationButton.GetComponent<Image>().sprite = defaultSprite;
				// clear tablets
				tabletCount.clearTablets();
				// update label and paper text
				labelText.UpdateLabelText();
				paperText.UpdatePaperText();
				// reactivate input and gui buttons
				foreach (GameObject gameObject in gameObjectsToDeactivateThenReactivate) {
					gameObject.SetActive(true);
				}
				// if was last wrong
				if (wasLastWrong) {
					// set timer
					stopwatchTimer.SetTimer(15f);
				}
			}
		}
	}

	// can be forced from stopwatch
	public void Confirm(bool forced = false) {
		if (tabletCount.numberOfTablets == dosage.numberOfTabletsNeeded) {
			// CORRECT
			confirmationButton.GetComponent<Image>().sprite = correctSprite;

			// increment correct counters
			++CorrectTotal;
			++CorrectInARow;

			// play sound
			audioSource.PlayOneShot(correct);
			// mark wasLastWrong flag as false
			wasLastWrong = false;
		}
		else {
			// if failed from stopwatch, instantly fail
			if (forced) {
				remainingFailures = 0;
			}
			--remainingFailures;

			// WRONG
			confirmationButton.GetComponent<Image>().sprite = wrongSprite;
			if (remainingFailures < 0) {
				result.text = "Too many failures";
			}

			// reset correct in a row
			CorrectInARow = 0;

			// play sound
			audioSource.PlayOneShot(wrong);
			// mark wasLastWrong flag as true
			wasLastWrong = true;
		}
		// deactivate input and gui buttons
		foreach (GameObject gameObject in gameObjectsToDeactivateThenReactivate) {
			gameObject.SetActive(false);
		}
		// deactivate confirmation button (we don't want it reenabled by this)
		confirmationButton.GetComponent<Button>().interactable = false;
		// start internal timer
		secondsUntilNextTurnTimer = secondsUntilNextTurn;
		// disable stopwatch timer
		stopwatchTimer.enabled = false;
	}
}
