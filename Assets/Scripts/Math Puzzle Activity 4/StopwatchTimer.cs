﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopwatchTimer : MonoBehaviour {
	[SerializeField]
	private RawImage stopwatch = null;
	[SerializeField]
	private RawImage stopwatchHand = null;
	[SerializeField]
	private ConfirmDosage confirmDosage = null;
	private AudioSource audioSource;
	private void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	[SerializeField]
	private float degreesPerSecond = 13.5f;

	private float timer = 0f;

	private void Update() {
		// decrease timer
		timer -= Time.deltaTime;
		// if timer has reached 0
		if (timer <= 0f) {
			// confirm the current dosage (will disable self)
			confirmDosage.Confirm(true);
		}
		// update angle of UI hand
		Vector3 angle = stopwatchHand.rectTransform.rotation.eulerAngles;
		angle.z = -degreesPerSecond * timer;
		stopwatchHand.rectTransform.rotation = Quaternion.Euler(angle);
	}

	public void SetTimer(float seconds) {
		timer = seconds;
		enabled = true;
		// play sound
		audioSource.Play();
	}

	public void OnDisable() {
		// hide stopwatch
		stopwatch.enabled = false;
		stopwatchHand.enabled = false;
	}

	public void OnEnable() {
		// show stopwatch
		stopwatch.enabled = true;
		stopwatchHand.enabled = true;
	}
}