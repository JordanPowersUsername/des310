﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dosage : MonoBehaviour {
	[SerializeField]
	private int[] possibleTotalDosagesInMilligrams = null;

	public int randomTotalDosageInMilligrams { get; private set; } = 100;

	public int individualDosageInMilligrams { get; private set; } = 10;

	public int numberOfTabletsNeeded { get; private set; } = 10;

	[SerializeField]
	private TabletCount tabletCount = null;



	[SerializeField]
	private int minimumNumberOfTablets = 4;

	private void Awake() {
		int maximumNumberOfTablets = tabletCount.getMaximumNumberOfTablets();
		// ensure that all possible total dosages can be split up into smaller tablets
		List<int> possibleNumbersOfTablets;
		foreach (int totalDosage in possibleTotalDosagesInMilligrams) {
			possibleNumbersOfTablets = GetPossibleNumbersOfTablets(totalDosage, minimumNumberOfTablets, maximumNumberOfTablets);
			if (possibleNumbersOfTablets.Count == 0) {
				print("ERROR: " + totalDosage + "mg cannot be divided into a smaller number of tablets between " + minimumNumberOfTablets + " and " + maximumNumberOfTablets);
			}
		}

		SetRandomDosage();
	}

	public void SetRandomDosage() {
		int maximumNumberOfTablets = tabletCount.getMaximumNumberOfTablets();
		// set random total dosage
		int totalDosageIndex = Random.Range(0, possibleTotalDosagesInMilligrams.Length);
		randomTotalDosageInMilligrams = possibleTotalDosagesInMilligrams[totalDosageIndex];
		// set random individual dosage that is a factor or the total dosage

		// get all possible numbers of tablets
		List<int> possibleNumbersOfTablets = GetPossibleNumbersOfTablets(randomTotalDosageInMilligrams, minimumNumberOfTablets, maximumNumberOfTablets);

		// pick a random number of tablets, and use it to calculate the individual dosage
		int possibleNumbersOfTabletsIndex = Random.Range(0, possibleNumbersOfTablets.Count);
		numberOfTabletsNeeded = possibleNumbersOfTablets[possibleNumbersOfTabletsIndex];
		individualDosageInMilligrams = randomTotalDosageInMilligrams / numberOfTabletsNeeded;
	}

	private List<int> GetPossibleNumbersOfTablets(int totalDosage, int minimumNumberOfTablets, int maximumNumberOfTablets) {
		List<int> possibleNumbersOfTablets = new List<int>();
		for (int numberOfTablets = minimumNumberOfTablets; numberOfTablets <= maximumNumberOfTablets; ++numberOfTablets) {
			// if quotient is an integer
			if (totalDosage % numberOfTablets == 0) {
				possibleNumbersOfTablets.Add(numberOfTablets);
			}
		}
		return possibleNumbersOfTablets;
	}
}
