﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewOnTap : MonoBehaviour {
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private ViewObject viewObject = null;
	[SerializeField]
	private Vector3 targetPosition = Vector3.zero;
	[SerializeField]
	private Vector3 targetRotation = Vector3.zero;
	[SerializeField]
	private Vector3 targetScale = Vector3.one;
	[SerializeField]
	private AudioClip onMoveAudio = null;

	new private Collider collider;
	private void Awake() {
		collider = GetComponent<Collider>();
	}

	private void Update() {
		// if screen is tapped, and the tap ray intersects the collider
		if (inputManager.state == InputManager.State.Tap
			&& collider.Raycast(inputManager.touchRay, out _, float.MaxValue)) {
			// consume the tap
			inputManager.ConsumeTap();
			// set the object to view as this object
			viewObject.SetObjectToView(gameObject, targetPosition, Quaternion.Euler(targetRotation), targetScale, onMoveAudio);
		}
	}
}
