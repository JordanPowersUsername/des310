﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VasePickUp : MonoBehaviour, ISaveableAndLoadable {
	[SerializeField]
	RoomManager roomManager = null;
	[SerializeField]
	GameObject vase = null;
	PickUp vasePickUp;
	VaseRenderTexture vaseRenderTexture;
	[SerializeField]
	PickUpHolder pickUpHolder = null;
	[SerializeField]
	int sortingOrderAfterPickedUp = 0;

	bool pickedUp = false;

	IngameButton ingameButton;
	BoxCollider2D boxCollider2D;
	SetSpriteSortingOrderBasedOnHeight setSpriteSortingOrderBasedOnHeight;
	SpriteRenderer spriteRenderer;

	Vector2 defaultPosition;
	string defaultRoom;
	void Awake() {
		ingameButton = GetComponent<IngameButton>();
		boxCollider2D = GetComponent<BoxCollider2D>();
		setSpriteSortingOrderBasedOnHeight = GetComponent<SetSpriteSortingOrderBasedOnHeight>();
		spriteRenderer = GetComponent<SpriteRenderer>();

		vasePickUp = vase.GetComponent<PickUp>();
		vaseRenderTexture = vase.GetComponent<VaseRenderTexture>();
		// default position and default room is the position and parent this is set to when loading and not picked up
		defaultPosition = transform.position;
		defaultRoom = transform.parent.name;
	}

	public void TryPickUp() {
		// if Vase is not already picked up and there is no room in the vase's set row of the inventory
		if ((!vasePickUp.PickedUp) && (!pickUpHolder.HasFreeSlot(vasePickUp.TopRow))) {
			// we can't pick up this part, return
			return;
		}
		// set picked up to true
		pickedUp = true;
		// set parent to vase (to be rendered into sprite)
		transform.SetParent(vase.transform);
		transform.localPosition = Vector2.zero;
		// set layer to RenderTexture (so that it's not visible by main camera, but is visible by vase's camera)
		gameObject.layer = LayerMask.NameToLayer("RenderTexture");
		// disable ingame button, box collider and sprite sorting order based on height
		ingameButton.enabled = false;
		boxCollider2D.enabled = false; // box collider needed to be set to false as no longer in Ignore Raycast layer
		setSpriteSortingOrderBasedOnHeight.enabled = false;
		// manually set sorting order (since order is important for final vase sprite)
		spriteRenderer.sortingOrder = sortingOrderAfterPickedUp;
		// if vase is not picked up try to pick it up
		if (!vasePickUp.PickedUp) {
			vasePickUp.TryPickUp();
		}
		// update vase's sprite
		vaseRenderTexture.UpdateSprite();
	}

	// Save and Load whether this is picked up or not
	[System.Serializable]
	struct VasePickUpStruct {
		public bool pickedUp;
	}

	public string Save() {
		VasePickUpStruct vasePickUpStruct = new VasePickUpStruct();
		vasePickUpStruct.pickedUp = pickedUp;
		return JsonUtility.ToJson(vasePickUpStruct);
	}
	public void Load(string json) {
		VasePickUpStruct vasePickUpStruct = JsonUtility.FromJson<VasePickUpStruct>(json);
		// if it was picked up, pick it up again
		if (vasePickUpStruct.pickedUp) {
			TryPickUp();
		}
		// otherwise, move it back to it's default room and position
		else {
			// set picked up to false
			pickedUp = false;
			// set parent to room
			transform.SetParent(roomManager.FindRoom(defaultRoom));
			transform.position = defaultPosition;
			// set layer to Ignore Raycast (so that it's is visible by main camera and raycast's ignore it, but is not visible by vase's camera)
			gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
			// enable ingame button, box collider and set sprite sorting order based on height
			ingameButton.enabled = true;
			boxCollider2D.enabled = true;
			setSpriteSortingOrderBasedOnHeight.enabled = true;
			// update vase's sprite
			vaseRenderTexture.UpdateSprite();
		}
	}
}
