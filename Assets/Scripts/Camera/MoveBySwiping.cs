﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBySwiping : MonoBehaviour {
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private float sensitivity = 0.1f;
	[SerializeField]
	private OpenInventory openInventory = null;
	[SerializeField]
	private GameStateManager gameStateManager = null;
	[SerializeField]
	private RoomManager roomManager = null;
	[SerializeField]
	private float cameraClampSpeed = 5f;

	private PolygonCollider2D cameraClamp;

	new private Camera camera;
	void Awake() {
		camera = GetComponent<Camera>();
	}

	private void Start() {
		FindCameraClamp();
	}

	void Update() {
		Vector3 cameraPosition = camera.transform.position;
		if (inputManager.state == InputManager.State.Swipe &&
			!openInventory.isTouched &&
			(gameStateManager.state == GameStateManager.GameState.Movement ||
			gameStateManager.state == GameStateManager.GameState.MoveToTarget ||
			gameStateManager.state == GameStateManager.GameState.MoveToTargetUninterruptible)) {

			// move camera with swipe
			Vector2 delta = inputManager.GetSwipeDeltaPosition() * sensitivity;
			cameraPosition.x -= delta.x;
			cameraPosition.y -= delta.y;
		}
		// clamp camera position to room hitbox
		// if outside of camera clamp
		if (!cameraClamp.OverlapPoint(cameraPosition)) {
			Vector3 closestPoint = cameraClamp.ClosestPoint(cameraPosition);
			closestPoint.z = cameraPosition.z;
			// lerp back in
			cameraPosition = Vector3.Lerp(cameraPosition, closestPoint, cameraClampSpeed * Time.deltaTime);
		}
		camera.transform.position = cameraPosition;
	}

	public void FindCameraClamp() {
		foreach (Transform child in roomManager.room.transform) {
			if (child.CompareTag("Camera Clamp")) {
				cameraClamp = child.GetComponent<PolygonCollider2D>();
				return;
			}
		}
	}
}
