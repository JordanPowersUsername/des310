﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOrthographicSizeByPinching : MonoBehaviour {
	[SerializeField]
	private InputManager inputManager = null;
	[SerializeField]
	private float sensitivity = 0.1f;
	[SerializeField]
	private float maximumZoom = 5f;
	[SerializeField]
	private float minimumZoom = 2f;
	[SerializeField]
	private GameStateManager gameStateManager = null;

	new private Camera camera;
	void Awake() {
		camera = GetComponent<Camera>();
	}

	void Update() {
		if (inputManager.state == InputManager.State.Pinch &&
			(gameStateManager.state == GameStateManager.GameState.Movement ||
			gameStateManager.state == GameStateManager.GameState.MoveToTarget ||
			gameStateManager.state == GameStateManager.GameState.MoveToTargetUninterruptible)) {
			camera.orthographicSize += -inputManager.pinchDeltaDistance * sensitivity;
		}
		// clamp camera zoom
		camera.orthographicSize = Mathf.Clamp(camera.orthographicSize, minimumZoom, maximumZoom);
	}
}
