﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {
	public Vector3 target = Vector3.zero;

	new Camera camera;
	void Awake() {
		camera = GetComponent<Camera>();
	}

	void Update() {
		camera.transform.LookAt(target, Vector3.back);
	}
}
