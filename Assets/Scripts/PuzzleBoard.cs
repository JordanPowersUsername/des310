﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleBoard : MonoBehaviour
{
	[SerializeField]
	private FlagManager flagManager = null;
	[SerializeField]
	private ChangeScene changeScene = null;
	[SerializeField]
	private EnergyManager energyManager = null;

	public void CheckIfCanPuzzle() {
		if (!energyManager.TryUseUpEnergy(1)) {
			energyManager.LockOut();
			return;
		}
		if (flagManager.GetFlag("Accountant") == 2) {
			changeScene.LoadScene("MathPuzzleActivity4Scene");
		}
		else if (flagManager.GetFlag("Accountant") == 5) {
			changeScene.LoadScene("MathPuzzleActivity5Scene");
		}
	}
}
