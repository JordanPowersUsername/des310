﻿Shader "Custom/Haunted"
{
    Properties
    {
        [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
        _Color("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0
        _Amplitude("Amplitude", Float) = 1
    }

        SubShader
        {
            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Cull Off
            Lighting Off
            ZWrite Off
            Blend One OneMinusSrcAlpha

            Pass
            {
            CGPROGRAM
                #pragma vertex SpriteVert
                #pragma fragment HauntedSpriteFrag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                #include "UnitySprites.cginc"
                    float _Amplitude;
                    fixed4 HauntedSpriteFrag(v2f IN) : SV_Target {
                        float2 texcoord = IN.texcoord;
                        texcoord.x += _Amplitude * (0.0075 * sin(20 * (texcoord.x + _Time)) + 0.004 * sin(45 * (texcoord.y + _Time + 0.1*sin(_Time))));
                        texcoord.y += _Amplitude * (0.01 * cos(22.5 * (texcoord.y + _Time)) + 0.004 * sin(47.5 * (texcoord.x + _Time + 0.1 * sin(_Time))));
                        fixed4 c = SampleSpriteTexture(texcoord) * IN.color;
                        c.r = saturate(c.r + _Amplitude * (0.025 + 0.05 * sin(_Time * 10) + 0.02 * sin(_Time * 10)));
                        c.g = saturate(c.g + _Amplitude * (0.025 + 0.05 * sin(_Time * 10) + 0.02 * sin(_Time * 9)));
                        c.b = saturate(c.b + _Amplitude * (0.025 + 0.05 * sin(_Time * 10) + 0.02 * sin(_Time * 8)));
                        c.a = saturate(c.a + _Amplitude * (0.15 * sin(_Time * 11) - 0.25));
                        c.rgb *= c.a;
                        return c;
                    }
            ENDCG
            }
        }
}
