﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// easter egg in here? what no.
// I need some recreation/relaxation
// this is how I choose to spend it

public class easterEgg : MonoBehaviour {
	[SerializeField]
	Sprite easterEggSprite = null;

	SpriteRenderer spriteRenderer;
	void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		if (Random.Range(0, 1337) == 0) {
			spriteRenderer.sprite = easterEggSprite;
		}
	}
	public void ForceEasterEgg() {
		spriteRenderer.sprite = easterEggSprite;
	}
}
